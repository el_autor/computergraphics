from tkinter import*
import numpy
import time

class MyCanvas:

    def __init__(self):
        self.mainWindow = Tk()
        self.mainWindow.geometry("1200x1000+700+25")
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Отрисовка")

        self.widthCanvas = 1200
        self.heightCanvas = 1000
        
        self.backgroundColor = "#ffffff"
        self.fillColor = "#000000"

        self.backgroundColorRGB = (255, 255, 255)
        self.fillColorRGB = (0, 0, 0)

        self.areaData = [[]]
        self.areaShapesTotal = 0

        self.canvas = Canvas(self.mainWindow, width = self.widthCanvas, height = self.heightCanvas, bg = "white")
        self.canvas.place(x = 0, y = 0)

        self.mainImage = PhotoImage(height = self.heightCanvas, width = self.widthCanvas)
        self.canvas.create_image((self.widthCanvas // 2, self.heightCanvas // 2), image = self.mainImage, state = "normal")

        self.fillBackground()

    def brezenhamInt(self, canvas, pointStart, pointEnd, lineColour):
        change = 0

        dx = pointEnd[0] - pointStart[0]
        dy = pointEnd[1] - pointStart[1]

        signX = numpy.sign(dx)
        signY = numpy.sign(dy)

        dx = abs(dx)
        dy = abs(dy)

        if (dx > dy):
            change = 0
        else:
            temp = dx
            dx = dy
            dy = temp
            change = 1
        
        error = 2 * dy - dx # Отличие от вещественного алгоритма
        deltaErrorX = 2 * dx
        deltaErrorY = 2 * dy 
        
        x = round(pointStart[0])
        y = round(pointStart[1])

        for i in numpy.arange(0, dx, 1):
            canvas.put(lineColour, to = (x, y))

            if (error >= 0):
                if (change == 1):
                    x += signX
                else:
                    y += signY

                error -= deltaErrorX
        
            if (change == 0):
                x += signX
            else:
                y += signY
                
            error += deltaErrorY

    def getIntersections(self, pointStart, pointEnd):
        exchange = False

        if (pointStart[0] > pointEnd[0]):
            exchange = True

        points = []
        change = 0

        dx = pointEnd[0] - pointStart[0]
        dy = pointEnd[1] - pointStart[1]

        signX = numpy.sign(dx)
        signY = numpy.sign(dy)

        dx = abs(dx)
        dy = abs(dy)

        if (dx > dy):
            change = 0
        else:
            temp = dx
            dx = dy
            dy = temp
            change = 1
        
        error = 2 * dy - dx
        deltaErrorX = 2 * dx
        deltaErrorY = 2 * dy 
        
        x = round(pointStart[0])
        y = round(pointStart[1])        

        if exchange:
            points.append([x, y])

        for i in numpy.arange(0, dx, 1):
            point1 = [x, y]

            if (error >= 0):
                if (change == 1):
                    x += signX
                else:
                    y += signY

                error -= deltaErrorX
        
            if (change == 0):
                x += signX
            else:
                y += signY

            point2 = [x, y]

            if abs(point1[1] - point2[1]) == 1:
                if (not exchange):
                    points.append(point1)
                else:
                    points.append(point2)
                    
            error += deltaErrorY

        if not exchange:
            points.append([x, y])

        return points

    def fillBackground(self):
        self.mainImage.put(self.backgroundColor, to = (0, 0, self.widthCanvas, self.heightCanvas))

    def addTopPoint(self, event):
        self.mainImage.put(self.fillColor, to = (event.x, event.y))
        self.areaData[self.areaShapesTotal].append([event.x, event.y])
        topPoints = self.areaData[self.areaShapesTotal]

        if (len(topPoints) > 1):
            self.brezenhamInt(self.mainImage, topPoints[len(topPoints) - 2], topPoints[len(topPoints) - 1], self.fillColor)

    def lastPoint(self, event):
        print(len(self.areaData))
        topPoints = self.areaData[self.areaShapesTotal]
        topPoints.append(topPoints[0])
        print(len(topPoints))
        self.brezenhamInt(self.mainImage, topPoints[len(topPoints) - 2], topPoints[len(topPoints) - 1], self.fillColor)
        self.areaData.append([])
        self.areaShapesTotal += 1

    def getColor(self, x, y):
        #print(self.mainImage.get(x, y))
        #time.sleep(1)
        return self.mainImage.get(x, y)

    def setColor(self, x, y, color):
        #print(color)
        #print(x, y)
        self.mainImage.put(color, to = (x, y))

    def colorsAreEqual(self, color1, color2):
        if (color1[0] == color2[0] and color1[1] == color2[1] and color1[2] == color2[2]):
            return True
        else:
            return False

    def getExtremums(self, points):
        extremums = []
        points.append(points[1])

        for i in range(1, len(points) - 1, 1):
            if points[i - 1][1] > points[i][1] and points[i][1] < points[i + 1][1]:
                extremums.append(True)
            elif points[i - 1][1] < points[i][1] and points[i][1] > points[i + 1][1]:
                extremums.append(True)
            else:
                extremums.append(False)

        del points[-1]
        return extremums

    def getRightmostPoint(self, data):
        rightmostX = data[0][0][0]

        for i in range(len(data) - 1):
            for j in range(len(data[i])):
                if (data[i][j][0] > rightmostX):
                    rightmostX = data[i][j][0]
                
        return rightmostX
    '''
    def horEndpoints(self, points):
        horEndpoints = []
        points.append(points[1])

        print(points)

        for i in range(len(points) - 2):
            if points[i + 1][1] == points[i + 2][1]:
                horEndpoints.append(True)
            else:
                horEndpoints.append(False)

        del points[-1]

        print(horEndpoints)

        return horEndpoints
    '''

    def fill(self, stoppage):
        
        # Находим крайнюю правую точку многоугольника
        rightmostPoint = self.getRightmostPoint(self.areaData)

        for i in range(len(self.areaData) - 1):
            shape = self.areaData[i]
            edgePoints = []

            for j in range(len(shape) - 1):
                # Если ребро горизонтальное, то не рассматриваем его
                extremums = self.getExtremums(shape)

                if (shape[j][1] == shape[j + 1][1]):
                    continue
                else:
                    # Получаем перечесения текущего ребра со сканирующими строками
                    edgePoints = self.getIntersections(shape[j], shape[j + 1])

                    # Проверяем - является ли точка локальным экстремумом
                    if (not extremums[j]):
                        edgePoints = edgePoints[:-1]
                    
                    for k in range(len(edgePoints)):
                        yPoint = edgePoints[k][1]
                        xPoint = edgePoints[k][0]
                        x = xPoint + 1

                        while (x <= rightmostPoint):
                            if (self.colorsAreEqual(self.getColor(x, yPoint), self.backgroundColorRGB)):
                                self.setColor(x, yPoint, self.fillColor)
                            else:
                                self.setColor(x, yPoint, self.backgroundColor)

                            self.canvas.update_idletasks()
                            x += 1
                        
                        if (stoppage):
                            time.sleep(0.6)