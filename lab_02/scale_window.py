from tkinter import*
from tkinter.messagebox import*
import canvas
import checks

class ScaleWindow:

    def __init__(self, canvas):
        self.scaleWindow = Tk()
        self.scaleWindow.geometry("300x200+280+110")
        self.scaleWindow.title("Масштабирование")
        self.scaleWindow.resizable(False, False)

        self.scaleCenterDescX = Label(self.scaleWindow, text = "Центр(x)")
        self.scaleCenterDescY = Label(self.scaleWindow, text = "Центр(y)")
        self.scaleCenterDescX.place(x = 50, y = 10)
        self.scaleCenterDescY.place(x = 190, y = 10)

        self.scaleCenterEntryX = Entry(self.scaleWindow, width = 10, bg = "white")
        self.scaleCenterEntryY = Entry(self.scaleWindow, width = 10, bg = "white")
        self.scaleCenterEntryX.place(x = 40, y = 40)
        self.scaleCenterEntryY.place(x = 180, y = 40)

        self.kLabelX = Label(self.scaleWindow, text = "kx")
        self.kLabelY = Label(self.scaleWindow, text = "ky")
        self.kLabelX.place(x = 70, y = 80)
        self.kLabelY.place(x = 210, y = 80)

        self.kEntryX = Entry(self.scaleWindow, width = 10, bg = "white")
        self.kEntryY = Entry(self.scaleWindow, width = 10, bg = "white")
        self.kEntryX.place(x = 40, y = 110)
        self.kEntryY.place(x = 180, y = 110)

        self.scaleButton = Button(self.scaleWindow, text = "Масштабировать", width = 20, height = 2, bd = 2, command = self.add_scale)
        self.scaleButton.place(x = 65, y = 145)

        self.canvasHandler = canvas

    def add_scale(self):
        centerX = self.scaleCenterEntryX.get()
        centerY = self.scaleCenterEntryY.get()
        kX = self.kEntryX.get()
        kY = self.kEntryY.get()

        if (not checks.is_float(centerX) or not checks.is_float(centerY) or not checks.is_float(kX) or not checks.is_float(kY)):
            showwarning("Предупреждение", "Должны быть введены вещесвенные числа!")
        else:
            centerX = float(centerX)
            centerY = self.canvasHandler.heightCanvas - float(centerY)
            kX = float(kX)
            kY = float(kY)
            newState = []

            for i in range(len(self.canvasHandler.objectsData)):
                name = self.canvasHandler.objectsData[i].get_name() 
                points = []

                for j in range(len(self.canvasHandler.objectsData[i]._basePoints_)):
                    point = []
                    point.append(kX * self.canvasHandler.objectsData[i]._basePoints_[j][0] + (1 - kX) * centerX) 
                    point.append(kY * self.canvasHandler.objectsData[i]._basePoints_[j][1] + (1 - kY) * centerY)
                    points.append(point)

                if (name == "line"):
                    line = canvas.Line("line", points, self.canvasHandler.canvas)
                    newState.append(line)

                elif (name == "oval"):
                    oval = canvas.Oval("oval", points, self.canvasHandler.canvas)
                    newState.append(oval)

                elif (name == "polygon"):
                    polygon = canvas.Polygon("polygon", points, self.canvasHandler.canvas)
                    newState.append(polygon)

            self.canvasHandler.canvas.delete("all")
            self.canvasHandler.objectsData = newState
            self.canvasHandler.history.append(newState)
            self.canvasHandler.draw(newState)
            self.canvasHandler.dataIndex += 1
            
            centerPointState = []
            centerPointState.append(kX * self.canvasHandler.centerPoint[0] + (1 - kX) * centerX)
            centerPointState.append(kY * self.canvasHandler.centerPoint[1] + (1 - kY) * centerY)
            self.canvasHandler.centralPointHistory.append(centerPointState)
            self.canvasHandler.centerPoint = centerPointState
            self.canvasHandler.place_center_point()