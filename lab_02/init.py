from tkinter import*
from tkinter.messagebox import*
import move_window
import scale_window
import turn_window

class Window:

    def __init__(self, canvas):
        self.mainWindow = Tk()
        self.mainWindow.geometry('400x400+100+350')
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Лаборторная работа №2") 

        self.mainFrame = Frame(self.mainWindow, width = 380, height = 380, relief = RAISED, borderwidth = 3, bg = "black")
        self.mainFrame.place(x = 10, y = 10)

        self.labelDescription = Label(self.mainFrame, text = "Операции", fg = "white", bg = "black", font = "Arial 12")
        self.labelDescription.place(x = 150, y = 10)

        self.moveButton = Button(self.mainFrame, text = "Перенос", bg = "white", bd = 2, width = 30, height = 3, command = self.move)
        self.moveButton.place(x = 70, y = 50)

        self.scalingButton = Button(self.mainFrame, text = "Масштабирование", bg = "white", bd = 2, width = 30, height = 3, command = self.scale)
        self.scalingButton.place(x = 70, y = 130)

        self.turnButton = Button(self.mainFrame, text = "Поворот", bg = "white", bd = 2, width = 30, height = 3, command = self.turn)
        self.turnButton.place(x = 70, y = 210)

        self.backButton = Button(self.mainFrame, text = "Откат назад", bg = "white", bd = 2, width = 20, height = 3, command = self.get_prev)
        self.backButton.place(x = 15, y = 290)

        self.forwardButton = Button(self.mainFrame, text = "В начальное состояние", bg = "white", bd = 2, width = 20, height = 3, command = self.get_init)
        self.forwardButton.place(x = 195, y = 290)

        self.canvasHandler = canvas

    def move(self):
        moveWindow = move_window.MoveWindow(self.canvasHandler)

    def scale(self):
        scaleWindow = scale_window.ScaleWindow(self.canvasHandler)

    def turn(self):
        turnWindow = turn_window.TurnWindow(self.canvasHandler)

    def get_prev(self):
        if (self.canvasHandler.dataIndex == 0):
            showwarning("Предупреждение", "Вы на начальном состоянии!")
        else:    
            self.canvasHandler.canvas.delete("all")
            self.canvasHandler.draw(self.canvasHandler.history[self.canvasHandler.dataIndex - 1])
            del self.canvasHandler.history[self.canvasHandler.dataIndex]
            del self.canvasHandler.centralPointHistory[self.canvasHandler.dataIndex]
            self.canvasHandler.dataIndex -= 1
            self.canvasHandler.objectsData = self.canvasHandler.history[self.canvasHandler.dataIndex]
            self.canvasHandler.centerPoint = self.canvasHandler.centralPointHistory[self.canvasHandler.dataIndex]
            self.canvasHandler.place_center_point()

    def get_init(self):
        if (self.canvasHandler.dataIndex == 0):
            showwarning("Предупреждение", "Вы находитесь в начальном состоянии!")
        else:
            self.canvasHandler.canvas.delete("all")
            self.canvasHandler.draw(self.canvasHandler.history[0])
            self.canvasHandler.history = self.canvasHandler.history[0:1]
            self.canvasHandler.centralPointHistory = self.canvasHandler.centralPointHistory[0:1]
            self.canvasHandler.dataIndex = 0
            self.canvasHandler.objectsData = self.canvasHandler.history[0]
            self.canvasHandler.centerPoint = self.canvasHandler.centralPointHistory[0]
            self.canvasHandler.place_center_point()

    def loop(self):
        self.mainWindow.mainloop()