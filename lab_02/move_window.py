from tkinter import*
from tkinter.messagebox import*
import canvas
import checks

class MoveWindow:

    def __init__(self, canvas):
        self.moveWindow = Tk()
        self.moveWindow.geometry("200x150+60+110")
        self.moveWindow.title("Перенос")
        self.moveWindow.resizable(False, False)

        self.dxLabel = Label(self.moveWindow, text = "dx")
        self.dyLabel = Label(self.moveWindow, text = "dy")
        self.dxLabel.place(x = 45, y = 15)
        self.dyLabel.place(x = 135, y = 15)

        self.dxEntry = Entry(self.moveWindow, width = 10, bg = "white")
        self.dyEntry = Entry(self.moveWindow, width = 10, bg = "white") 
        self.dxEntry.place(x = 15, y = 40)
        self.dyEntry.place(x = 105, y = 40)

        self.moveButton = Button(self.moveWindow, text = "Перенести", width = 20, height = 2, bd = 2, command = self.add_move)
        self.moveButton.place(x = 15, y = 80)

        self.canvasHandler = canvas
    
    def add_move(self):
        dx = self.dxEntry.get()
        dy = self.dyEntry.get()

        if (not checks.is_float(dx) or not checks.is_float(dy)):
            showwarning("Предупреждение", "Введите вещественные смещения")
        else:
            newState = []

            for i in range(len(self.canvasHandler.objectsData)):
                name = self.canvasHandler.objectsData[i].get_name() 
                points = []

                for j in range(len(self.canvasHandler.objectsData[i]._basePoints_)):
                    point = []
                    point.append(self.canvasHandler.objectsData[i]._basePoints_[j][0] + float(dx))
                    point.append(self.canvasHandler.objectsData[i]._basePoints_[j][1] - float(dy))
                    points.append(point)

                if (name == "line"):
                    line = canvas.Line("line", points, self.canvasHandler.canvas)
                    newState.append(line)

                elif (name == "oval"):
                    oval = canvas.Oval("oval", points, self.canvasHandler.canvas)
                    newState.append(oval)

                elif (name == "polygon"):
                    polygon = canvas.Polygon("polygon", points, self.canvasHandler.canvas)
                    newState.append(polygon)

            self.canvasHandler.canvas.delete("all")
            self.canvasHandler.objectsData = newState
            self.canvasHandler.history.append(newState)
            self.canvasHandler.draw(newState)
            self.canvasHandler.dataIndex += 1
            
            centerPointState = []
            centerPointState.append(self.canvasHandler.centerPoint[0] + float(dx))
            centerPointState.append(self.canvasHandler.centerPoint[1] - float(dy))
            self.canvasHandler.centralPointHistory.append(centerPointState)
            self.canvasHandler.centerPoint = centerPointState
            self.canvasHandler.place_center_point()