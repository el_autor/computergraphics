from tkinter import*
from tkinter.messagebox import*
import canvas
import checks
import math

class TurnWindow:

    def __init__(self, canvas):
        self.turnWindow = Tk()
        self.turnWindow.geometry("300x200+130+800")
        self.turnWindow.title("Поворот")
        self.turnWindow.resizable(False, False)

        self.centerLabelX = Label(self.turnWindow, text = "Центр(x)")
        self.centerLabelY = Label(self.turnWindow, text = "Центр(y)")
        self.centerLabelX.place(x = 50, y = 10)
        self.centerLabelY.place(x = 190, y = 10)

        self.centerEntryX = Entry(self.turnWindow, width = 10, bg = "white")
        self.centerEntryY = Entry(self.turnWindow, width = 10, bg = "white")
        self.centerEntryX.place(x = 40, y = 40)
        self.centerEntryY.place(x = 180, y = 40)

        self.angleLabel = Label(self.turnWindow, text = "Угол(целый градус)")
        self.angleLabel.place(x = 95, y = 70)

        self.angleEntry = Entry(self.turnWindow, width = 10, bg = "white")
        self.angleEntry.place(x = 115, y = 100)

        self.turnButton = Button(self.turnWindow, text = "Повернуть", width = 20, height = 2, bd = 2, command = self.add_turn)
        self.turnButton.place(x = 65, y = 135)

        self.canvasHandler = canvas

    def add_turn(self):
        centerX = self.centerEntryX.get()
        centerY = self.centerEntryY.get()
        angle = self.angleEntry.get()

        if (not checks.is_float(centerX) or not checks.is_float(centerY) or not checks.is_int(angle)):
            showwarning("Предупреждение", "Должны быть введены вещественные числа!")
        else:
            centerX = float(centerX)
            centerY = self.canvasHandler.heightCanvas - float(centerY)
            angle = ((int(angle) * math.pi) / 180)
            newState = []

            for i in range(len(self.canvasHandler.objectsData)):
                name = self.canvasHandler.objectsData[i].get_name() 
                points = []

                for j in range(len(self.canvasHandler.objectsData[i]._basePoints_)):
                    point = []
                    point.append(centerX + (self.canvasHandler.objectsData[i]._basePoints_[j][0] - centerX) * math.cos(angle) + (self.canvasHandler.objectsData[i]._basePoints_[j][1] - centerY) * math.sin(angle))
                    point.append(centerY + (self.canvasHandler.objectsData[i]._basePoints_[j][1] - centerY) * math.cos(angle) + (centerX - self.canvasHandler.objectsData[i]._basePoints_[j][0]) * math.sin(angle))
                    points.append(point)

                if (name == "line"):
                    line = canvas.Line("line", points, self.canvasHandler.canvas)
                    newState.append(line)

                elif (name == "oval"):
                    oval = canvas.Oval("oval", points, self.canvasHandler.canvas)
                    newState.append(oval)

                elif (name == "polygon"):
                    polygon = canvas.Polygon("polygon", points, self.canvasHandler.canvas)
                    newState.append(polygon)

            self.canvasHandler.canvas.delete("all")
            self.canvasHandler.objectsData = newState
            self.canvasHandler.history.append(newState)
            self.canvasHandler.draw(newState)
            self.canvasHandler.dataIndex += 1
            
            centerPointState = []
            centerPointState.append(centerX + (self.canvasHandler.centerPoint[0] - centerX) * math.cos(angle) + (self.canvasHandler.centerPoint[1] - centerY) * math.sin(angle))
            centerPointState.append(centerY + (self.canvasHandler.centerPoint[1] - centerY) * math.cos(angle) + (centerX - self.canvasHandler.centerPoint[0]) * math.sin(angle))
            self.canvasHandler.centralPointHistory.append(centerPointState)
            self.canvasHandler.centerPoint = centerPointState
            self.canvasHandler.place_center_point()