from tkinter import*
import math
import numpy

class CanvasObject:

    def __init__(self, name, basePoints, baseCanvas):
        self._name_ = name
        self._basePoints_ = basePoints
        self._baseCanvas_ = baseCanvas

    def get_name(self):
        return self._name_

    def draw(self):
        pass

class Line(CanvasObject):

    def draw(self):
        self._baseCanvas_.create_line(round(self._basePoints_[0][0]), round(self._basePoints_[0][1]), round(self._basePoints_[1][0]), round(self._basePoints_[1][1]), width = 3)

class Polygon(CanvasObject):

    def draw(self):
        for i in range(len(self._basePoints_) - 1):
            self._baseCanvas_.create_line(round(self._basePoints_[i][0]), round(self._basePoints_[i][1]), round(self._basePoints_[i + 1][0]), round(self._basePoints_[i + 1][1]), width = 3)

        self._baseCanvas_.create_line(round(self._basePoints_[len(self._basePoints_) - 1][0]), round(self._basePoints_[len(self._basePoints_) - 1][1]), round(self._basePoints_[0][0]), round(self._basePoints_[0][1]), width = 3)        

class Oval(CanvasObject):

    def draw(self):
        for i in range(len(self._basePoints_)):
            self._baseCanvas_.create_oval(round(self._basePoints_[i][0] - 1), round(self._basePoints_[i][1] - 1), round(self._basePoints_[i][0] + 1), round(self._basePoints_[i][1] + 1), fill = "black")        

class MyCanvas:

    def __init__(self):
        self.mainWindow = Tk()
        self.mainWindow.geometry("1200x1000+600+25")
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Рисунок")

        self.widthCanvas = 1198
        self.heightCanvas = 998

        self.canvas = Canvas(self.mainWindow, width = self.widthCanvas, height = self.heightCanvas, bg = "white")
        self.canvas.place(x = 0, y = 0)

        self.centerPoint = [600, 500]
        self.centralPointLabel = Label(self.mainWindow, bg = "white")
        self.place_center_point()

        # Тут лежат все объекты рисования(их опорные точки)
        self.objectsData = self.init_data()

        self.draw(self.objectsData)

        # История изменений
        self.centralPointHistory = [self.centerPoint]
        self.history = [self.objectsData]
        self.dataIndex = 0

    def place_center_point(self):
        self.canvas.create_oval(round(self.centerPoint[0] - 1), round(self.centerPoint[1] - 1), round(self.centerPoint[0] + 1), round(self.centerPoint[1] + 1), fill = "red", outline = "red")
        self.centralPointLabel['text'] = "(" + str(round(self.centerPoint[0])) + ", " + str(round(self.heightCanvas - self.centerPoint[1])) + ")"
        self.centralPointLabel.place(x = self.centerPoint[0] + 5, y = self.centerPoint[1] - 20)

    def init_data(self):
        data = []

        houseRectangle = Polygon("polygon", [[200, 300], [1000, 300], [1000, 900], [200, 900]], self.canvas)
        data.append(houseRectangle)

        houseRoofLeft = Line("line", [[200, 300], [600, 100]], self.canvas)
        houseRoofRight = Line("line", [[600, 100], [1000, 300]], self.canvas)
        data.append(houseRoofLeft)
        data.append(houseRoofRight)

        leftWindowPoints = []
        leftWindowR = 80
        step = 0.001
        deltaX = 400
        deltaY = 450

        for i in numpy.arange(0, math.pi * 2, step):
            pointX = leftWindowR * math.cos(i) + deltaX 
            pointY = leftWindowR * math.sin(i) + deltaY
            leftWindowPoints.append([pointX, pointY]) 
        
        leftWindow = Oval("oval", leftWindowPoints, self.canvas)
        data.append(leftWindow)

        windowHorStick = Line("line", [[deltaX - leftWindowR, deltaY], [deltaX + leftWindowR, deltaY]], self.canvas)
        windowVertStick = Line("line", [[deltaX, deltaY - leftWindowR], [deltaX, deltaY + leftWindowR]], self.canvas)

        data.append(windowHorStick)
        data.append(windowVertStick)

        roofWindow = Polygon("polygon",[[520, 170], [680, 170], [680, 270], [520, 270]], self.canvas)
        data.append(roofWindow)

        roofWindowHorStick = Line("line", [[600, 170], [600, 270]], self.canvas)
        roofWindowVertStick = Line("line", [[520, 220], [680, 220]], self.canvas)
        data.append(roofWindowHorStick)
        data.append(roofWindowVertStick)

        rightWindowPoints = []
        rightWindowHorAxis = 100
        rightWindowVertAxis = 200
        deltaX = 800
        deltaY = 600

        for i in numpy.arange(0, math.pi * 2, step):
            pointX = rightWindowHorAxis * math.cos(i) + deltaX
            pointY = rightWindowVertAxis * math.sin(i) + deltaY
            rightWindowPoints.append([pointX, pointY])

        rightWindow = Oval("oval", rightWindowPoints, self.canvas)
        data.append(rightWindow)

        rightWindowFrame = Polygon("polygon", [[deltaX - rightWindowHorAxis, deltaY], [deltaX, deltaY - rightWindowVertAxis], [deltaX + rightWindowHorAxis, deltaY], [deltaX, deltaY + rightWindowVertAxis]], self.canvas)
        data.append(rightWindowFrame)

        rightWindowHorStick = Line("line", [[deltaX - rightWindowHorAxis, deltaY], [deltaX + rightWindowHorAxis, deltaY]], self.canvas)
        rightWindowVertStick = Line("line", [[deltaX, deltaY - rightWindowVertAxis], [deltaX, deltaY + rightWindowVertAxis]], self.canvas)
        data.append(rightWindowHorStick)
        data.append(rightWindowVertStick)

        return data

    def draw(self, objectsData):
        for i in range(len(objectsData)):
            objectsData[i].draw()