from tkinter import*
from tkinter.messagebox import*
from tkinter.colorchooser import askcolor
import time

class Window:

    def __init__(self, canvasWnd):
        self.mainWindow = Tk()
        self.mainWindow.geometry('500x570+100+300')
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Лабораторная работа №5")

        self.initFigureButton = Button(self.mainWindow, text = "Начать рисование", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.drawPerimeter)
        self.initFigureButton.place(x = 160, y = 150)   

        self.chooseColorButton = Button(self.mainWindow, text = "Выбрать цвет", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.setColor)
        self.chooseColorButton.place(x = 160, y = 230)

        self.drawButton = Button(self.mainWindow, text = "Закрасить", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.fillShape)
        self.drawButton.place(x = 160, y = 280)

        self.drawDelayButton = Button(self.mainWindow, text = "Закрасить с задержкой", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.fillShapeStoppage)
        self.drawDelayButton.place(x = 160, y = 330)    

        self.cleanButton = Button(self.mainWindow, text = "Очистка", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.resetBackground)
        self.cleanButton.place(x = 160, y = 380)

        self.canvasHandler = canvasWnd

        self.colorIndicator = IntVar(self.mainWindow)
        self.colorIndicator.set(0)
        self.colorIndicatorBg = Radiobutton(self.mainWindow, text = "- фон", variable = self.colorIndicator, value = 0)
        self.colorIndicatorFill = Radiobutton(self.mainWindow, text = "- закраска", variable = self.colorIndicator, value = 1)
        self.colorIndicatorLine = Radiobutton(self.mainWindow, text = "- линия", variable = self.colorIndicator, value = 2)
        self.colorIndicatorBg.place(x = 120, y = 205)
        self.colorIndicatorFill.place(x = 205, y = 205)
        self.colorIndicatorLine.place(x = 320, y = 205)

    def fillShape(self):
        start = time.time()
        self.canvasHandler.fill(False)
        end = time.time()
        showinfo("Заполнение завершено", "Время: " + str(end - start))        

    def fillShapeStoppage(self):
        self.canvasHandler.fill(True)

    def drawPerimeter(self):
        self.canvasHandler.canvas.bind("<Button 1>", self.canvasHandler.addTopPoint)
        self.canvasHandler.canvas.bind("<Button 2>", self.canvasHandler.setInitPixel)
        self.canvasHandler.canvas.bind("<Button 3>", self.canvasHandler.lastPoint)

    def setColor(self):
        color = askcolor(parent = self.mainWindow)
        if self.colorIndicator.get() == 0:
            self.canvasHandler.backgroundColorRGB = (int(color[0][0]), int(color[0][1]),int(color[0][2]))
            self.canvasHandler.backgroundColor = color[1]
            self.canvasHandler.fillBackground()
        elif self.colorIndicator.get() == 1:
            self.canvasHandler.fillColorRGB = int(color[0][0]), int(color[0][1]),int(color[0][2])
            self.canvasHandler.fillColor = color[1]
        else:
            self.canvasHandler.lineColorRGB = int(color[0][0]), int(color[0][1]),int(color[0][2])
            self.canvasHandler.lineColor = color[1]

    def resetBackground(self):
        self.canvasHandler.fillBackground()
        self.canvasHandler.areaData = [[]]
        self.canvasHandler.initPixel = []
        self.canvasHandler.areaShapesTotal = 0
        self.canvasHandler.canvas.unbind("<Button 1>")
        self.canvasHandler.canvas.unbind("<Button 2>")
        self.canvasHandler.canvas.unbind("<Button 3>")

    def loop(self):
        self.mainWindow.mainloop()