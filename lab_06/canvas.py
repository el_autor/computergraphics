from tkinter import*
import time
import numpy
from stack import Stack

class MyCanvas:

    def __init__(self):
        self.mainWindow = Tk()
        self.mainWindow.geometry("1200x1000+700+25")
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Отрисовка")

        self.widthCanvas = 1200
        self.heightCanvas = 1000
        
        self.backgroundColor = "#ffffff"
        self.fillColor = "#000000"
        self.lineColor = "#0404d9"

        self.backgroundColorRGB = (255, 255, 255)
        self.fillColorRGB = (0, 0, 0)
        self.lineColorRGB = (4, 4, 217)

        self.areaData = [[]]
        self.areaShapesTotal = 0

        self.canvas = Canvas(self.mainWindow, width = self.widthCanvas, height = self.heightCanvas, bg = "white")
        self.canvas.place(x = 0, y = 0)

        self.mainImage = PhotoImage(height = self.heightCanvas, width = self.widthCanvas)
        self.canvas.create_image((self.widthCanvas // 2, self.heightCanvas // 2), image = self.mainImage, state = "normal")

        self.fillBackground()

    def brezenhamInt(self, canvas, pointStart, pointEnd, lineColour):
        change = 0

        dx = pointEnd[0] - pointStart[0]
        dy = pointEnd[1] - pointStart[1]

        signX = numpy.sign(dx)
        signY = numpy.sign(dy)

        dx = abs(dx)
        dy = abs(dy)

        if (dx > dy):
            change = 0
        else:
            temp = dx
            dx = dy
            dy = temp
            change = 1
        
        error = 2 * dy - dx # Отличие от вещественного алгоритма
        deltaErrorX = 2 * dx
        deltaErrorY = 2 * dy 
        
        x = round(pointStart[0])
        y = round(pointStart[1])

        for i in range(0, dx):
            canvas.put(lineColour, to = (x, y))

            if (error >= 0):
                if (change == 1):
                    x += signX
                else:
                    y += signY

                error -= deltaErrorX
        
            if (change == 0):
                x += signX
            else:
                y += signY
                
            error += deltaErrorY

    def fillBackground(self):
        self.mainImage.put(self.backgroundColor, to = (0, 0, self.widthCanvas, self.heightCanvas))

    def addTopPoint(self, event):
        self.mainImage.put(self.fillColor, to = (event.x, event.y))
        self.areaData[self.areaShapesTotal].append([event.x, event.y])
        topPoints = self.areaData[self.areaShapesTotal]

        if (len(topPoints) > 1):
            self.brezenhamInt(self.mainImage, topPoints[len(topPoints) - 2], topPoints[len(topPoints) - 1], self.lineColor)

    def lastPoint(self, event):
        print(len(self.areaData))
        topPoints = self.areaData[self.areaShapesTotal]
        topPoints.append(topPoints[0])
        print(len(topPoints))
        self.brezenhamInt(self.mainImage, topPoints[len(topPoints) - 2], topPoints[len(topPoints) - 1], self.lineColor)
        self.areaData.append([])
        self.areaShapesTotal += 1

    def setInitPixel(self, event):
        self.initPixel = [event.x, event.y] 

    def getColor(self, x, y):
        #print(self.mainImage.get(x, y))
        #time.sleep(1)
        return self.mainImage.get(x, y)

    def setPixel(self, x, y, color):
        #print(color)
        #print(x, y)
        self.mainImage.put(color, to = (x, y))

    def colorsAreEqual(self, color1, color2):
        if (color1[0] == color2[0] and color1[1] == color2[1] and color1[2] == color2[2]):
            return True
        else:
            return False

    def fill(self, stoppage):
        # Инициализируем стек и кладем туда затравочный пиксель
        stack = Stack()
        stack.push(self.initPixel)

        while (not stack.isEmpty()):
            pixel = stack.pop()
            x, y = pixel[0], pixel[1]
            self.setPixel(x, y, self.fillColor)
            self.canvas.update_idletasks()            

            tempX = x
            x += 1

            while (not self.colorsAreEqual(self.getColor(x, y), self.lineColorRGB)):
                self.setPixel(x, y, self.fillColor)
                self.canvas.update_idletasks()
                x += 1

            xRight = x - 1
            x = tempX - 1

            while (not self.colorsAreEqual(self.getColor(x, y), self.lineColorRGB)):
                self.setPixel(x, y, self.fillColor)
                self.canvas.update_idletasks()
                x -= 1

            if stoppage:
                time.sleep(0.3)

            xLeft = x + 1                

            # Проверяем строку выше
            x = xLeft
            y += 1

            while (x <= xRight):
                flag = 0

                while (not self.colorsAreEqual(self.getColor(x, y), self.lineColorRGB) and
                       not self.colorsAreEqual(self.getColor(x, y), self.fillColorRGB) and
                       x <= xRight):
                    flag = 1
                    x += 1
                
                if (flag == 1):
                    if (x == xRight and
                        not self.colorsAreEqual(self.getColor(x, y), self.lineColorRGB) and
                        not self.colorsAreEqual(self.getColor(x, y), self.fillColorRGB)):
                        stack.push([x, y])
                    else:
                        stack.push([x - 1, y])

                # Продолжаем проверку, если интервал был прерван
                xEntry = x

                while ((self.colorsAreEqual(self.getColor(x, y), self.lineColorRGB) or
                       self.colorsAreEqual(self.getColor(x, y), self.fillColorRGB)) and
                       x < xRight):
                    x += 1

                if (x == xEntry):
                    x += 1

        
            # Проверяем строку ниже
            x = xLeft
            y -= 2

            while (x <= xRight):
                flag = 0

                while (not self.colorsAreEqual(self.getColor(x, y), self.lineColorRGB) and
                       not self.colorsAreEqual(self.getColor(x, y), self.fillColorRGB) and
                       x <= xRight):
                    flag = 1
                    x += 1
                
                if (flag == 1):
                    if (x == xRight and
                        not self.colorsAreEqual(self.getColor(x, y), self.lineColorRGB) and
                        not self.colorsAreEqual(self.getColor(x, y), self.fillColorRGB)):
                        stack.push([x, y])
                    else:
                        stack.push([x - 1, y])

                # Продолжаем проверку, если интервал был прерван
                xEntry = x

                while ((self.colorsAreEqual(self.getColor(x, y), self.lineColorRGB) or
                       self.colorsAreEqual(self.getColor(x, y), self.fillColorRGB)) and
                       x < xRight):
                    x += 1

                if (x == xEntry):
                    x += 1