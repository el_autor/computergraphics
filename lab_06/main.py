from canvas import MyCanvas
from central_window import Window

if __name__ == "__main__":
    canvas = MyCanvas()
    myApp = Window(canvas)
    myApp.loop()