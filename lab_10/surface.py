import math

class Surface:

    def __init__(self, a ,b):
        self._a_ = a
        self._b_ = b 

    def value(self, x, z):
        return float(x) / 5 - (float(z) / 5) * (math.cos(z) * 10) - 5