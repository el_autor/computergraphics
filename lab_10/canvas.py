from tkinter import*
from tkinter.messagebox import*
import time
import numpy
from math import*
import time

class MyCanvas:

    def __init__(self):
        self.mainWindow = Tk()
        self.mainWindow.geometry("1200x1000+700+25")
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Отрисовка")

        self.widthCanvas = 1200
        self.heightCanvas = 1000

        self.lineColor = "#000000"
        self.debugColor = "#a3a3a3"
        self.backgroundColor = "#ffffff"

        self.lineColorRGB = (0, 0, 0)
        self.cutterColorRGB = (0, 0, 0)
        self.cutLineColorRGB = (4, 4, 217)

        self.canvas = Canvas(self.mainWindow, width = self.widthCanvas, height = self.heightCanvas, bg = "white")
        self.canvas.place(x = 0, y = 0)

        self.canvas.bind("<Button 1>", self.showCoords)

        self.mainImage = PhotoImage(height = self.heightCanvas, width = self.widthCanvas)
        self.canvas.create_image((self.widthCanvas // 2, self.heightCanvas // 2), image = self.mainImage, state = "normal")

        self.fillBackground()

    def showCoords(self, event):
        print(event.x, event.y)

    def brezenhamInt(self, canvas, pointStart, pointEnd, lineColour):
        change = 0

        pointStart = [round(pointStart[0]), round(pointStart[1])]
        pointEnd = [round(pointEnd[0]), round(pointEnd[1])]

        dx = pointEnd[0] - pointStart[0]
        dy = pointEnd[1] - pointStart[1]

        signX = numpy.sign(dx)
        signY = numpy.sign(dy)

        dx = abs(dx)
        dy = abs(dy)

        if (dx > dy):
            change = 0
        else:
            temp = dx
            dx = dy
            dy = temp
            change = 1
        
        error = 2 * dy - dx # Отличие от вещественного алгоритма
        deltaErrorX = 2 * dx
        deltaErrorY = 2 * dy 
        
        x = round(pointStart[0])
        y = round(pointStart[1])

        for i in range(0, dx + 1):
            canvas.put(lineColour, to = (x, y))

            if (error >= 0):
                if (change == 1):
                    x += signX
                else:
                    y += signY

                error -= deltaErrorX
        
            if (change == 0):
                x += signX
            else:
                y += signY
                
            error += deltaErrorY

    def fillBackground(self):
        self.mainImage.put(self.backgroundColor, to = (0, 0, self.widthCanvas, self.heightCanvas))

    def rotate(self, point, z, angles):
        point = numpy.array([[point[0], point[1], z, 1]])
        xRotation = numpy.array([[1, 0, 0, 0],
                                 [0, cos(angles[0]), sin(angles[0]), 0],
                                 [0, -sin(angles[0]), cos(angles[0]), 0],
                                 [0, 0, 0, 1]])
        
        point = point.dot(xRotation)

        yRotation = numpy.array([[cos(angles[1]), 0, -sin(angles[1]), 0],
                                 [0, 1, 0, 0],
                                 [sin(angles[1]), 0, cos(angles[1]), 0],
                                 [0, 0, 0, 1]])

        point = point.dot(yRotation)

        zRotation = numpy.array([[cos(angles[2]), sin(angles[2]), 0, 0],
                                 [-sin(angles[2]), cos(angles[2]), 0, 0],
                                 [0, 0, 1, 0],
                                 [0, 0, 0, 1]])

        point = point.dot(zRotation)        

        return [round(float(point[0][0])), round(float(point[0][1]))]

    def build(self, surfaceObj, xArgs, zArgs, angles):
        upHorizont = {}
        downHorizont = {}

        for x in range(xArgs[0], xArgs[1], xArgs[2]):
            yValue = surfaceObj.value(x, zArgs[0])
            point = self.rotate([x, yValue], zArgs[0], angles)
            xNew = round(point[0])
            upHorizont[xNew] = round(point[1])
            downHorizont[xNew] = round(point[1])

            if (x != xArgs[0]):
                self.brezenhamInt(self.mainImage, [xNew - 1 + 500, last + 500], [xNew + 500, upHorizont[xNew] + 500], self.lineColor)

            last = upHorizont[xNew]

        for z in range(zArgs[0], zArgs[1] + 1, zArgs[2]):
            if z == zArgs[0]:
                leftEdgeLocal = [xArgs[0], surfaceObj.value(xArgs[0], z)]
                rightEdgeLocal = [xArgs[1], surfaceObj.value(xArgs[1], z)]
                leftEdgeLocal = self.rotate(leftEdgeLocal, z, angles)
                rightEdgeLocal = self.rotate(rightEdgeLocal, z, angles)
            else:
                currentLeft = self.rotate([xArgs[0], surfaceObj.value(xArgs[0], z)], z, angles)
                currentRight = self.rotate([xArgs[1], surfaceObj.value(xArgs[1], z)], z, angles)

                if currentLeft[0] not in upHorizont or currentLeft[0] not in downHorizont:
                    self.brezenhamInt(self.mainImage, [leftEdgeLocal[0] + 500, leftEdgeLocal[1] + 500], [currentLeft[0] + 500, currentLeft[1] + 500], self.lineColor)
                elif (currentLeft[1] > upHorizont[currentLeft[0]] or currentLeft[1] < downHorizont[currentLeft[0]]):
                    self.brezenhamInt(self.mainImage, [leftEdgeLocal[0] + 500, leftEdgeLocal[1] + 500], [currentLeft[0] + 500, currentLeft[1] + 500], self.lineColor) 
                
                leftEdgeLocal = currentLeft

                if currentRight[0] not in upHorizont or currentRight[0] not in downHorizont:
                    self.brezenhamInt(self.mainImage, [rightEdgeLocal[0] + 500, rightEdgeLocal[1] + 500], [currentRight[0] + 500, currentRight[1] + 500], self.lineColor)
                elif (currentRight[1] > upHorizont[currentRight[0]] or currentRight[1] < downHorizont[currentRight[0]]):
                    self.brezenhamInt(self.mainImage, [rightEdgeLocal[0] + 500, rightEdgeLocal[1] + 500], [currentRight[0] + 500, currentRight[1] + 500], self.lineColor)    

                rightEdgeLocal = currentRight

            prevUpVisibility = False
            prevDownVisibility = False
            last = [0, 0]
            
            for x in range(xArgs[0], xArgs[1] + 1, xArgs[2]):
                upVisibility = False
                downVisibility = False
                y = surfaceObj.value(x, z)
                point = self.rotate([x, y], z, angles)
                y = point[1]
                xNew = round(point[0])

                if xNew in upHorizont:
                    if (round(y) > upHorizont[xNew]):
                        upVisibility = True
                        upHorizont[xNew] = round(y)
                    elif (round(y) < downHorizont[xNew]):
                        downVisibility = True
                        downHorizont[xNew] = round(y)
                else:
                    upHorizont[xNew] = round(y)
                    downHorizont[xNew] = round(y)

                if x != xArgs[0]: 

                    if (xArgs[2] != 1):
                        if (not prevUpVisibility) and upVisibility:
                            last = self.getIntersection(last, point, upHorizont)
                        elif prevUpVisibility and (not upVisibility):
                            point = self.getIntersection(last, point, upHorizont)
                        elif (not prevDownVisibility) and downVisibility:
                            last = self.getIntersection(last, point, downHorizont)
                        elif prevDownVisibility and (not downVisibility):
                            point = self.getIntersection(last, point, downHorizont)

                    if prevDownVisibility or prevUpVisibility or downVisibility or upVisibility:
                        self.brezenhamInt(self.mainImage, [last[0] + 500, last[1] + 500], [point[0] + 500, point[1] + 500], self.lineColor)

                prevUpVisibility = upVisibility
                prevDownVisibility = downVisibility
                last[0] = point[0]
                last[1] = point[1]

    def getIntersection(self, p1, p2, horizont):
        if p1[0] not in horizont:
            horizont[p1[0]] = self.approximateHorizont(p1[0], horizont)

        k1 = (p2[1] - p1[1]) / float(p2[0] - p1[0])
        k2 = (horizont[p2[0]] - horizont[p1[0]]) / float(p2[0] - p1[0])          

        x = p1[0] + (horizont[p2[0]] - p1[1]) / (k1 - k2)
        y = p1[1] + k1 * (x - p1[0])

        horizont[round(x)] = round(y)

        return [round(x), round(y)]  

    def approximateHorizont(self, x, horizont):
        left = x
        right = x

        while (left not in horizont):
            left -= 1

        while (right not in horizont):
            right += 1

        return round((right - left) / float(x - left) * (horizont[right] - horizont[left]) + horizont[left])
