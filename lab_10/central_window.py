from tkinter import*
from tkinter.messagebox import*
from tkinter.colorchooser import askcolor
from surface import Surface
import math
import time

class Window:

    def __init__(self, canvasWnd):
        self.mainWindow = Tk()
        self.mainWindow.geometry('500x450+100+300')
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Лабораторная работа №10")

        self.surfaceLabel = Label(self.mainWindow, text = "Поверхность  -")
        self.surfaceLabel.place(x = 110, y = 25)

        self.surfaceList = ["Эллипсоид", "Another"]
        self.surfaceVar = StringVar(self.mainWindow)
        self.surfaceVar.set(self.surfaceList[0])
        self.surfaceMenu = OptionMenu(self.mainWindow, self.surfaceVar, *self.surfaceList)
        self.surfaceMenu.config(width = 20, bg = "white", bd = 1, relief = RIDGE)
        self.surfaceMenu.place(x = 220, y = 20)

        self.xParamsLabel = Label(self.mainWindow, text = "Параметры оси X")
        self.xParamsLabel.place(x = 180, y = 60)

        self.xIntervalLabel = Label(self.mainWindow, text = "Интервал")
        self.xIntervalLabel.place(x = 90, y = 80)

        self.xStepLabel = Label(self.mainWindow, text = "Шаг")
        self.xStepLabel.place(x = 350, y = 80)

        self.xStartEntry = Entry(self.mainWindow, width = 6, bd = 1)
        self.xStartEntry.place(x = 60, y = 100)

        self.xEndEntry = Entry(self.mainWindow, width = 6, bd = 1)
        self.xEndEntry.place(x = 140, y = 100)

        self.xStepEntry = Entry(self.mainWindow, width = 6, bd = 1)
        self.xStepEntry.place(x = 340, y = 100)

        self.zParamsLabel = Label(self.mainWindow, text = "Параметры оси Z")
        self.zParamsLabel.place(x = 180, y = 130)

        self.zIntervalLabel = Label(self.mainWindow, text = "Интервал")
        self.zIntervalLabel.place(x = 90, y = 150)

        self.zStepLabel = Label(self.mainWindow, text = "Шаг")
        self.zStepLabel.place(x = 350, y = 150)

        self.zStartEntry = Entry(self.mainWindow, width = 6, bd = 1)
        self.zStartEntry.place(x = 60, y = 170)

        self.zEndEntry = Entry(self.mainWindow, width = 6, bd = 1)
        self.zEndEntry.place(x = 140, y = 170)

        self.zStepEntry = Entry(self.mainWindow, width = 6, bd = 1)
        self.zStepEntry.place(x = 340, y = 170)

        self.drawSurfaceButton = Button(self.mainWindow, text = "Построить", bg = "white", width = 10, bd = 1, relief = RIDGE, command = self.buildSurface)
        self.drawSurfaceButton.place(x = 195, y = 200)

        self.rotateLabel = Label(self.mainWindow, text = "Параметры вращения")
        self.rotateLabel.place(x = 175, y = 240)
        
        self.xAxisLabel = Label(self.mainWindow, text = "Ось X")
        self.xAxisLabel.place(x = 130, y = 260)

        self.yAxisLabel = Label(self.mainWindow, text = "Ось Y")
        self.yAxisLabel.place(x = 230, y = 260)

        self.zAxisLabel = Label(self.mainWindow, text = "Ось Z")
        self.zAxisLabel.place(x = 330, y = 260)
        
        self.xAxisEntry = Entry(self.mainWindow, width = 6, bd = 1)
        self.xAxisEntry.place(x = 123, y = 280)

        self.yAxisEntry = Entry(self.mainWindow, width = 6, bd = 1)
        self.yAxisEntry.place(x = 223, y = 280)

        self.zAxisEntry = Entry(self.mainWindow, width = 6, bd = 1)
        self.zAxisEntry.place(x = 325, y = 280)

        self.rotateButton = Button(self.mainWindow, text = "Вращать", bg = "white", width = 10, bd = 1, relief = RIDGE, command = self.rotateSurface)
        self.rotateButton.place(x = 195, y = 310)

        self.scaleLabel = Label(self.mainWindow, text = "Коэф. масштабирования")
        self.scaleLabel.place(x = 30, y = 350)

        self.scaleEntry = Entry(self.mainWindow, width = 6, bd = 1)
        self.scaleEntry.place(x = 90, y = 370)

        self.scaleButton = Button(self.mainWindow, text = "Масштабировать", bg = "white", width = 13, bd = 1, relief = RIDGE)
        self.scaleButton.place(x = 300, y = 360)

        self.cleanButton = Button(self.mainWindow, text = "Очистка", bg = "white", width = 10, bd = 1, relief = RIDGE, command = self.clean)
        self.cleanButton.place(x = 195, y = 400)

        self.canvasHandler = canvasWnd

    def rotateSurface(self):
        xArgs = []
        xArgs.append(int(self.xStartEntry.get()))
        xArgs.append(int(self.xEndEntry.get()))
        xArgs.append(int(self.xStepEntry.get()))

        zArgs = []
        zArgs.append(int(self.zStartEntry.get()))
        zArgs.append(int(self.zEndEntry.get()))
        zArgs.append(int(self.zStepEntry.get()))
        
        surface = Surface(1, 1)

        angleX = (float(self.xAxisEntry.get()) / 180) * math.pi
        angleY = (float(self.yAxisEntry.get()) / 180) * math.pi
        angleZ = (float(self.zAxisEntry.get()) / 180) * math.pi

        self.canvasHandler.build(surface, xArgs, zArgs, [angleX, angleY, angleZ])

    def buildSurface(self):
        xArgs = []
        xArgs.append(int(self.xStartEntry.get()))
        xArgs.append(int(self.xEndEntry.get()))
        xArgs.append(int(self.xStepEntry.get()))

        zArgs = []
        zArgs.append(int(self.zStartEntry.get()))
        zArgs.append(int(self.zEndEntry.get()))
        zArgs.append(int(self.zStepEntry.get()))

        surface = Surface(1, 1)

        self.canvasHandler.build(surface, xArgs, zArgs, [0, 0, 0])
        print(zArgs)

    def clean(self):
        self.canvasHandler.fillBackground()

    def loop(self):
        self.mainWindow.mainloop()