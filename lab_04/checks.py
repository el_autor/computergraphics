def is_float(number):
    try:
        number = float(number)
    except ValueError:
        return False

    return True

def is_int(number):
    try:
        number = int(number)
    except ValueError:
        return False

    return True