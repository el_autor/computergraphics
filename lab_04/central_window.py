from tkinter import*
from checks import*
import calculations
from tkinter.messagebox import*
import matplotlib.pyplot as plot
import numpy as np
import math

class Window:

    def __init__(self, canvasWnd):
        self.mainWindow = Tk()
        self.mainWindow.geometry('500x570+100+300')
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Лаборторная работа №4") 

        self.mainFrame = Frame(self.mainWindow, width = 500, height = 570, relief = RAISED, borderwidth = 3, bg = "black")
        self.mainFrame.place(x = 0, y = 0)

        # Построение одиночных отрезков

        self.singleFigureFrame = Frame(self.mainFrame, width = 450, height = 170, relief = RAISED, borderwidth = 3, bg = "black")
        self.singleFigureFrame.place(x = 25, y = 80)

        self.algorithmLabel = Label(self.mainFrame, text = "Алгоритм", fg = "white", bg = "black")
        self.algorithmLabel.place(x = 80, y = 10)

        self.figureLabel = Label(self.mainFrame, text = "Тип фигуры", fg = "white", bg = "black")
        self.figureLabel.place(x = 240, y = 10) 

        self.colorLabel = Label(self.mainFrame, text = "Цвет", fg = "white", bg = "black")
        self.colorLabel.place(x = 395, y = 10)

        self.singleFigureLabel = Label(self.mainFrame, text = "Одиночная фигура", fg = "white", bg = "black")
        self.singleFigureLabel.place(x = 180, y = 85)

        self.centerPointLabel = Label(self.mainFrame, text = "Центр", fg = "white", bg = "black")
        self.centerPointLabel.place(x = 90, y = 110)

        self.semiAxisLengthLabel = Label(self.mainFrame, text = "Полуоси", fg = "white", bg = "black")
        self.semiAxisLengthLabel.place(x = 350, y = 110)

        self.centerPointLabelX = Label(self.mainFrame, text = "x", fg = "white", bg = "black")
        self.centerPointLabelX.place(x = 60, y = 130)

        self.centerPointLabelY = Label(self.mainFrame, text = "y", fg = "white", bg = "black")
        self.centerPointLabelY.place(x = 150, y = 130)

        self.semiAxisLengthLabelA = Label(self.mainFrame, text = "a", fg = "white", bg = "black")
        self.semiAxisLengthLabelA.place(x = 330, y = 130)

        self.semiAxisLengthLabelB = Label(self.mainFrame, text = "b", fg = "white", bg = "black")
        self.semiAxisLengthLabelB.place(x = 420, y = 130)

        self.centerPointEntryX = Entry(self.mainFrame, width = 6, bd = 1)
        self.centerPointEntryX.place(x = 40, y = 150)

        self.centerPointEntryY = Entry(self.mainFrame, width = 6, bd = 1)
        self.centerPointEntryY.place(x = 130, y = 150)    

        self.semiAxisLengthEntryA = Entry(self.mainFrame, width = 6, bd = 1)
        self.semiAxisLengthEntryA.place(x = 310, y = 150)
        
        self.semiAxisLengthEntryB = Entry(self.mainFrame, width = 6, bd = 1)
        self.semiAxisLengthEntryB.place(x = 400, y = 150)

        self.algorithmList = ["Канон. ур-ие", "Парам. ур-ие", "Брезенхем", "Средняя точка", "Библ. алг-м."]
        self.algorithmVar = StringVar(self.mainFrame)
        self.algorithmVar.set(self.algorithmList[0])
        self.algorithmMenu = OptionMenu(self.mainFrame, self.algorithmVar, *self.algorithmList)
        self.algorithmMenu.config(width = 19, bg = "white", bd = 1, relief = RIDGE)
        self.algorithmMenu.place(x = 25, y = 40)

        self.figureList = ["Окружность", "Эллипс"]
        self.figureVar = StringVar(self.mainFrame)
        self.figureVar.set(self.figureList[0])
        self.figureMenu = OptionMenu(self.mainFrame, self.figureVar, *self.figureList)
        self.figureMenu.config(width = 10, bg = "white", bd = 1, relief = RIDGE)
        self.figureMenu.place(x = 224, y = 40)

        self.colorList = ["Черный", "Белый", "Коричневый", "Зеленый", "Желтый", "Розовый"]
        self.colorDict = { "Черный" : "black", "Белый" : "white", "Коричневый" : "brown", "Зеленый" : "green", "Желтый": "yellow", "Розовый" : "pink"}
        self.colorVar = StringVar(self.mainFrame)
        self.colorVar.set(self.colorList[0])
        self.colorMenu = OptionMenu(self.mainFrame, self.colorVar, *self.colorList)
        self.colorMenu.config(width = 10, bg = "white", bd = 1, relief = RIDGE)
        self.colorMenu.place(x = 350, y = 40)

        self.drawSingleFigureButton = Button(self.mainFrame, text = "Нарисовать фигуру", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.drawSingleFigure)
        self.drawSingleFigureButton.place(x = 150, y = 190)

        # Рисоване спектра

        self.spectreFrame = Frame(self.mainFrame, width = 450, height = 235, relief = RAISED, borderwidth = 3, bg = "black")
        self.spectreFrame.place(x = 25, y = 265)

        self.spectreParamsLabel = Label(self.mainFrame, text = "Спектр фигур", fg = "white", bg = "black")
        self.spectreParamsLabel.place(x = 195, y = 270)

        self.centralSpectrePointLabel = Label(self.mainFrame, text = "Центр", fg = "white", bg = "black")
        self.centralSpectrePointLabel.place(x = 85, y = 290)

        self.centralSpectrePointLabelX = Label(self.mainFrame, text = "x", fg = "white", bg = "black")
        self.centralSpectrePointLabelX.place(x = 60, y = 310)

        self.centralSpectrePointLabelY = Label(self.mainFrame, text = "y", fg = "white", bg = "black")
        self.centralSpectrePointLabelY.place(x = 150, y = 310)

        self.deltaSemiAxisLabel = Label(self.mainFrame, text = "Шаг полуосей", fg = "white", bg = "black")
        self.deltaSemiAxisLabel.place(x = 330, y = 290)

        self.deltaSemiAxisLabelA = Label(self.mainFrame, text = "a", fg = "white", bg = "black")
        self.deltaSemiAxisLabelA.place(x = 330, y = 310)

        self.deltaSemiAxisLabelB = Label(self.mainFrame, text = "b", fg = "white", bg = "black")
        self.deltaSemiAxisLabelB.place(x = 420, y = 310)  

        self.initValuesSemiAxes = Label(self.mainFrame, text = "Нач. знач. полуосей", fg = "white", bg = "black") 
        self.initValuesSemiAxes.place(x = 39, y = 360)       

        self.figuresNumberLabel = Label(self.mainFrame, text = "Кол-во фигур", fg = "white", bg = "black")
        self.figuresNumberLabel.place(x = 330, y = 360)

        self.initValueSemiAxisLabelA = Label(self.mainFrame, text = "da", fg = "white", bg = "black")
        self.initValueSemiAxisLabelA.place(x = 56, y = 375)

        self.initValueSemiAxisLabelB = Label(self.mainFrame, text = "db", fg = "white", bg = "black")
        self.initValueSemiAxisLabelB.place(x = 146, y = 375)

        self.centralSpectrePointEntryX = Entry(self.mainFrame, width = 6, bd = 1)
        self.centralSpectrePointEntryX.place(x = 40, y = 330)

        self.centralSpectrePointEntryY = Entry(self.mainFrame, width = 6, bd = 1)
        self.centralSpectrePointEntryY.place(x = 130, y = 330)

        self.deltaSemiAxisEntryA = Entry(self.mainFrame, width = 6, bd = 1)
        self.deltaSemiAxisEntryA.place(x = 310, y = 330)

        self.deltaSemiAxisEntryB = Entry(self.mainFrame, width = 6, bd = 1)
        self.deltaSemiAxisEntryB.place(x = 400, y = 330)

        self.initValueSemiAxisEntryA = Entry(self.mainFrame, width = 6, bd = 1)
        self.initValueSemiAxisEntryA.place(x = 40, y = 395)

        self.initValueSemiAxisEntryB = Entry(self.mainFrame, width = 6, bd = 1) 
        self.initValueSemiAxisEntryB.place(x = 130, y = 395)

        self.figuresNumberEntry = Entry(self.mainFrame, width = 6, bd = 1)
        self.figuresNumberEntry.place(x = 354, y = 395)

        self.drawVisualParamsButton = Button(self.mainFrame, text = "Нарисовать спектр", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.drawVisualParams)
        self.drawVisualParamsButton.place(x = 150, y = 440)

        # Исследование временных характеристик

        self.timingParamsButton = Button(self.mainFrame, text = "Временные\nзатраты", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.getTiming)
        self.timingParamsButton.place(x = 25, y = 508)

        # Кнопка очистки

        self.cleanButton = Button(self.mainFrame, text = "Очистка", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.clear)
        self.cleanButton.place(x = 286, y = 508)

        self.canvasHandler = canvasWnd.canvas

    def drawSingleFigure(self):
        xCenter = self.centerPointEntryX.get()
        yCenter = self.centerPointEntryY.get()
        aAxis = self.semiAxisLengthEntryA.get()
        bAxis = self.semiAxisLengthEntryB.get()
        
        if (not is_float(xCenter) or not is_float(yCenter) or not is_float(aAxis) or not is_float(bAxis)):
            showwarning("Предупреждение", "Введите целые или вещественные числа!")
        else:
            if (self.figureVar.get() == "Окружность"):
                calculations.drawSingleCircle(self.canvasHandler, self.algorithmVar.get(), self.colorDict[self.colorVar.get()],
                                    [round(float(xCenter)), round(float(yCenter))], round(float(aAxis)))
            else:
                calculations.drawSingleEllipse(self.canvasHandler, self.algorithmVar.get(), self.colorDict[self.colorVar.get()],
                                    [round(float(xCenter)), round(float(yCenter))], round(float(aAxis)), round(float(bAxis)))

    def drawVisualParams(self):
        xCenter = self.centralSpectrePointEntryX.get()
        yCenter = self.centralSpectrePointEntryY.get()
        deltaSemiAxisA = self.deltaSemiAxisEntryA.get()
        deltaSemiAxisB = self.deltaSemiAxisEntryB.get()
        initValueSemiAxisA = self.initValueSemiAxisEntryA.get()
        initValueSemiAxisB = self.initValueSemiAxisEntryB.get()
        figuresNumber = self.figuresNumberEntry.get()

        if (not is_float(xCenter) or not is_float(yCenter) or
            not is_float(deltaSemiAxisA) or not is_float(deltaSemiAxisB) or
            not is_float(initValueSemiAxisA) or not is_float(initValueSemiAxisB) or
            not is_float(figuresNumber)):
            showwarning("Предупреждение", "Введите целые или вещественные числа!")
        else:
            if (self.figureVar.get() == "Окружность"):
                calculations.drawVisualParamsCircle(self.canvasHandler, self.algorithmVar.get(), self.colorDict[self.colorVar.get()],
                                                   [round(float(xCenter)), round(float(yCenter))],
                                                   round(float(initValueSemiAxisA)), round(float(deltaSemiAxisA)),
                                                   round(float(figuresNumber)))
            else:
                calculations.drawVisualParamsEllipse(self.canvasHandler, self.algorithmVar.get(), self.colorDict[self.colorVar.get()],
                                                   [round(float(xCenter)), round(float(yCenter))],
                                                   round(float(initValueSemiAxisA)), round(float(initValueSemiAxisB)), 
                                                   round(float(deltaSemiAxisA)), round(float(deltaSemiAxisB)),
                                                   round(float(figuresNumber)))

    def showTiming(self, timeInfo):
        if (len(timeInfo) > 0):
            x = np.arange(len(timeInfo))

            colors = np.random.rand(len(timeInfo), 3)
            plot.bar(x, height = timeInfo.values(), color = colors)
            plot.xticks(x, timeInfo.keys())
            plot.xlabel("Алгоритм")
            plot.ylabel("Время(c)")
            plot.title("Исследование затраченного времени")
            plot.show()

    def getTiming(self):
        xCenter = self.centralSpectrePointEntryX.get()
        yCenter = self.centralSpectrePointEntryY.get()
        deltaSemiAxisA = self.deltaSemiAxisEntryA.get()
        deltaSemiAxisB = self.deltaSemiAxisEntryB.get()
        initValueSemiAxisA = self.initValueSemiAxisEntryA.get()
        initValueSemiAxisB = self.initValueSemiAxisEntryB.get()
        figuresNumber = self.figuresNumberEntry.get()

        if (not is_float(xCenter) or not is_float(yCenter) or
            not is_float(deltaSemiAxisA) or not is_float(deltaSemiAxisB) or
            not is_float(initValueSemiAxisA) or not is_float(initValueSemiAxisB) or
            not is_float(figuresNumber)):
            showwarning("Предупреждение", "Введите целые или вещественные числа!")
        else:
            if (self.figureVar.get() == "Окружность"):
                timing = calculations.getTimingCircle(self.canvasHandler, self.algorithmList, self.colorDict[self.colorVar.get()],
                                                      [round(float(xCenter)), round(float(yCenter))],
                                                      round(float(initValueSemiAxisA)), round(float(deltaSemiAxisA)),
                                                      round(float(figuresNumber)))
            else:
                timing = calculations.getTimingEllipse(self.canvasHandler, self.algorithmList, self.colorDict[self.colorVar.get()],
                                                       [round(float(xCenter)), round(float(yCenter))],
                                                       round(float(initValueSemiAxisA)), round(float(initValueSemiAxisA)), 
                                                       round(float(deltaSemiAxisA)), round(float(deltaSemiAxisB)),
                                                       round(float(figuresNumber))) 

            self.showTiming(timing)

    def clear(self):
        self.canvasHandler.delete("all")

    def loop(self):
        self.mainWindow.mainloop()