import math
import numpy
import time

def canonicEquationCircle(canvas, centerPoint, r, color):
    xStartEight = centerPoint[0]
    xEndEight = centerPoint[0] + round(r / math.sqrt(2)) + 1
    midPointOneEightY = round(math.sqrt(r * r - (xEndEight - centerPoint[0]) *
                             (xEndEight - centerPoint[0])) + centerPoint[1])

    for x in range(xStartEight, xEndEight + 1, 1):
        # Рисуем 1/8

        yDownOneEigth = round(math.sqrt(r * r - (x - centerPoint[0]) *
                             (x - centerPoint[0])) + centerPoint[1]) 
        canvas.create_line(x, yDownOneEigth,
                           x + 1, yDownOneEigth + 1,
                           width = 1, fill = color)
        
        # Рисуем четверть

        xUpOneEight = xEndEight + abs(midPointOneEightY - yDownOneEigth)
        yUpOneEight = midPointOneEightY - abs(xEndEight - x)
        canvas.create_line(xUpOneEight, yUpOneEight,
                           xUpOneEight + 1, yUpOneEight + 1,
                           width = 1, fill = color)

        # Рисуем половину

        yOneHalfUp = yDownOneEigth - 2 * abs(centerPoint[1] - yDownOneEigth)
        yOneHalfDown = yUpOneEight - 2 * abs(centerPoint[1] - yUpOneEight)
        canvas.create_line(x, yOneHalfUp,
                           x + 1, yOneHalfUp + 1,
                           width = 1, fill = color)
        canvas.create_line(xUpOneEight, yOneHalfDown,
                           xUpOneEight + 1, yOneHalfDown + 1,
                           width = 1, fill = color)

        # Рисуем всю окружность

        xDownLeftDown = xStartEight - abs(xStartEight - x)
        xDownLeftUpDown = xStartEight - abs(xStartEight - xUpOneEight)
        canvas.create_line(xDownLeftDown, yDownOneEigth,
                           xDownLeftDown + 1, yDownOneEigth + 1,
                           width = 1, fill = color)
        canvas.create_line(xDownLeftUpDown, yUpOneEight,
                           xDownLeftUpDown + 1, yUpOneEight + 1,
                           width = 1, fill = color)
        canvas.create_line(xDownLeftDown, yOneHalfUp,
                           xDownLeftDown + 1,
                           yOneHalfUp + 1,
                           width = 1, fill = color)
        canvas.create_line(xDownLeftUpDown, yOneHalfDown,
                           xDownLeftUpDown + 1, yOneHalfDown + 1,
                           width = 1, fill = color)

def tkinterAlgorithmCircle(canvas, centerPoint, r, color):
    canvas.create_oval(centerPoint[0] - r, centerPoint[1] - r, centerPoint[0] + r, centerPoint[1] + r, width = 1, outline = color)

def parameterEquationCircle(canvas, centerPoint, r, color):
    xStartEight = centerPoint[0]
    xEndEight = centerPoint[0] + round(r / math.sqrt(2)) + 1
    midPointOneEightY = round(centerPoint[1] + r * math.sin(math.pi / 4))
    step = 1 / r
    start = math.pi / 4
    end = math.pi / 2

    for i in numpy.arange(start, end, step):
        # Рисуем 1/8

        x = round(centerPoint[0] + r * math.cos(i))
        yDownOneEigth = round(centerPoint[1] + r * math.sin(i))
        canvas.create_line(x, yDownOneEigth,
                           x + 1, yDownOneEigth + 1,
                           width = 1, fill = color)

        # Рисуем 1/4

        xUpOneEight = xEndEight + abs(midPointOneEightY - yDownOneEigth)
        yUpOneEight = midPointOneEightY - abs(xEndEight - x)
        canvas.create_line(xUpOneEight, yUpOneEight,
                           xUpOneEight + 1, yUpOneEight + 1,
                           width = 1, fill = color)

        # Рисуем 1/2

        yOneHalfUp = yDownOneEigth - 2 * abs(centerPoint[1] - yDownOneEigth)
        yOneHalfDown = yUpOneEight - 2 * abs(centerPoint[1] - yUpOneEight)
        canvas.create_line(x, yOneHalfUp,
                           x + 1, yOneHalfUp + 1,
                           width = 1, fill = color)
        canvas.create_line(xUpOneEight, yOneHalfDown,
                           xUpOneEight + 1, yOneHalfDown + 1,
                           width = 1, fill = color)

        # Рисуем всю окружность

        xDownLeftDown = xStartEight - abs(xStartEight - x)
        xDownLeftUpDown = xStartEight - abs(xStartEight - xUpOneEight)
        canvas.create_line(xDownLeftDown, yDownOneEigth,
                           xDownLeftDown + 1, yDownOneEigth + 1,
                           width = 1, fill = color)
        canvas.create_line(xDownLeftUpDown, yUpOneEight,
                           xDownLeftUpDown + 1, yUpOneEight + 1,
                           width = 1, fill = color)
        canvas.create_line(xDownLeftDown, yOneHalfUp,
                           xDownLeftDown + 1, yOneHalfUp + 1,
                           width = 1, fill = color)
        canvas.create_line(xDownLeftUpDown, yOneHalfDown,
                           xDownLeftUpDown + 1, yOneHalfDown + 1,
                           width = 1, fill = color)

def horStepCircle(x, delta):
    return x + 1, delta + 2 * x + 3

def diagStepCircle(x, y, delta):
    return x + 1, y - 1, delta + 2 * (x - y + 3)

def vertStepCircle(y, delta):
    return y - 1, delta - 2 * y + 3

def brezenhamCircle(canvas, centerPoint, r, color):
    oneEightsPoints = []
    xStartEight = 0
    xEndEight = round(r / math.sqrt(2)) + 1
    delta = 2 * (1 - r) 

    x = 0
    y = r

    for i in range(xStartEight, xEndEight + 1, 1):
        # Рисуем 1/8

        xDownOneEigth = x + centerPoint[0]
        yDownOneEigth = y + centerPoint[1]
        canvas.create_line(xDownOneEigth, yDownOneEigth,
                           xDownOneEigth + 1, yDownOneEigth + 1,
                           width = 1, fill = color)
        oneEightsPoints.append([xDownOneEigth, yDownOneEigth])

        if (delta < 0):
            delta1 = 2 * (delta + y) - 1

            if (delta1 <= 0):
                x, delta = horStepCircle(x, delta)
            else:
                x, y, delta = diagStepCircle(x, y, delta)

        elif (delta > 0):
            delta2 = 2 * (delta - x) - 1

            if (delta2 >= 0):
                y, delta = vertStepCircle(y, delta)
            else:
                x, y, delta = diagStepCircle(x, y, delta)

        else:
            x, y, delta = diagStepCircle(x, y, delta)

    totalPoints = len(oneEightsPoints)
    midPointOneEightY = oneEightsPoints[-1][1]
    xStartEight += centerPoint[0]
    xEndEight += centerPoint[0]

    for i in range(totalPoints):
        x = oneEightsPoints[i][0]
        yDownOneEigth = oneEightsPoints[i][1]

        # Рисуем четверть

        xUpOneEight = xEndEight + abs(midPointOneEightY - yDownOneEigth)
        yUpOneEight = midPointOneEightY - abs(xEndEight - x)
        canvas.create_line(xUpOneEight, yUpOneEight,
                           xUpOneEight + 1, yUpOneEight + 1,
                           width = 1, fill = color)

        # Рисуем половину

        yOneHalfUp = yDownOneEigth - 2 * abs(centerPoint[1] - yDownOneEigth)
        yOneHalfDown = yUpOneEight - 2 * abs(centerPoint[1] - yUpOneEight)
        canvas.create_line(x, yOneHalfUp,
                           x + 1, yOneHalfUp + 1,
                           width = 1, fill = color)
        canvas.create_line(xUpOneEight, yOneHalfDown,
                           xUpOneEight + 1, yOneHalfDown + 1,
                           width = 1, fill = color)

        # Рисуем всю окружность

        xDownLeftDown = xStartEight - abs(xStartEight - x)
        xDownLeftUpDown = xStartEight - abs(xStartEight - xUpOneEight)
        canvas.create_line(xDownLeftDown, yDownOneEigth,
                           xDownLeftDown + 1, yDownOneEigth + 1,
                           width = 1, fill = color)
        canvas.create_line(xDownLeftUpDown, yUpOneEight,
                           xDownLeftUpDown + 1, yUpOneEight + 1,
                           width = 1, fill = color)
        canvas.create_line(xDownLeftDown, yOneHalfUp, 
                           xDownLeftDown + 1, yOneHalfUp + 1,
                           width = 1, fill = color)
        canvas.create_line(xDownLeftUpDown, yOneHalfDown,
                           xDownLeftUpDown + 1, yOneHalfDown + 1,
                           width = 1, fill = color)

def midPointCircle(canvas, centerPoint, r, color):
    oneEightsPoints = []

    dx = 0
    dy = r
    funcTry = 5 / 4.0 - r

    # Рисуем 1/8

    while (dy >= dx):
        drawingX = dx + centerPoint[0] 
        drawingY = dy + centerPoint[1]

        canvas.create_line(drawingX, drawingY,
                           drawingX + 1, drawingY + 1,
                           width = 1, fill = color)
        oneEightsPoints.append([drawingX, drawingY])

        if (funcTry < 0):
            funcTry += (2 * dx + 1)
            dx += 1
        else:
            funcTry += 2 * dx + 1 - 2 * dy
            dx += 1
            dy -= 1

    totalPoints = len(oneEightsPoints)
    midPointOneEightY = oneEightsPoints[-1][1]
    xStartEight = oneEightsPoints[0][0]
    xEndEight = oneEightsPoints[-1][0]

    for i in range(totalPoints):
        x = oneEightsPoints[i][0]
        yDownOneEigth = oneEightsPoints[i][1]

        # Рисуем четверть

        xUpOneEight = xEndEight + abs(midPointOneEightY - yDownOneEigth)
        yUpOneEight = midPointOneEightY - abs(xEndEight - x)
        canvas.create_line(xUpOneEight, yUpOneEight,
                           xUpOneEight + 1, yUpOneEight + 1,
                           width = 1, fill = color)

        # Рисуем половину

        yOneHalfUp = yDownOneEigth - 2 * abs(centerPoint[1] - yDownOneEigth)
        yOneHalfDown = yUpOneEight - 2 * abs(centerPoint[1] - yUpOneEight)
        canvas.create_line(x, yOneHalfUp,
                           x + 1, yOneHalfUp + 1,
                           width = 1, fill = color)
        canvas.create_line(xUpOneEight, yOneHalfDown,
                           xUpOneEight + 1, yOneHalfDown + 1,
                           width = 1, fill = color)

        # Рисуем всю окружность

        xDownLeftDown = xStartEight - abs(xStartEight - x)
        xDownLeftUpDown = xStartEight - abs(xStartEight - xUpOneEight)
        canvas.create_line(xDownLeftDown, yDownOneEigth,
                           xDownLeftDown + 1, yDownOneEigth + 1,
                           width = 1, fill = color)
        canvas.create_line(xDownLeftUpDown, yUpOneEight,
                           xDownLeftUpDown + 1, yUpOneEight + 1,
                           width = 1, fill = color)
        canvas.create_line(xDownLeftDown, yOneHalfUp,
                           xDownLeftDown + 1, yOneHalfUp + 1,
                           width = 1, fill = color)
        canvas.create_line(xDownLeftUpDown, yOneHalfDown,
                           xDownLeftUpDown + 1, yOneHalfDown + 1,
                           width = 1, fill = color)
    
def drawSingleCircle(canvas, algorithmType, color, centerPoint, r):
    if algorithmType == "Канон. ур-ие":

        canonicEquationCircle(canvas, centerPoint, r, color)
        
    elif algorithmType == "Парам. ур-ие":

        parameterEquationCircle(canvas, centerPoint, r, color)

    elif algorithmType == "Брезенхем":

        brezenhamCircle(canvas, centerPoint, r, color)

    elif algorithmType == "Средняя точка":

        midPointCircle(canvas, centerPoint, r, color)

    elif algorithmType == "Библ. алг-м.":

        tkinterAlgorithmCircle(canvas, centerPoint, r, color)

def canonicEquationEllipse(canvas, centerPoint, aAxisLength, bAxisLength, color):
    oneForthPoints = []

    x = centerPoint[0]
    y = centerPoint[1] + bAxisLength

    aSquared = aAxisLength * aAxisLength
    bSquared = bAxisLength * bAxisLength

    # Рисуем четверть

    while (abs(x - centerPoint[0]) * bSquared < abs(y - centerPoint[1]) * aSquared):
        oneForthPoints.append([round(x), round(y)])
        canvas.create_line(x, y,
                           x + 1, y + 1,
                           width = 1, fill = color)
        x += 1
        y = round(math.sqrt(bSquared * (1 - ((x - centerPoint[0]) *
                 (x - centerPoint[0])) / aSquared)) + centerPoint[1])

    while (y >= centerPoint[1]):
        oneForthPoints.append([round(x), round(y)])
        canvas.create_line(x, y,
                           x + 1, y + 1,
                           width = 1, fill = color)
        y -= 1
        x = round(math.sqrt(aSquared * (1 - ((y - centerPoint[1]) *
                 (y - centerPoint[1])) / bSquared)) + centerPoint[0])

    totalPoints = len(oneForthPoints)

    for i in range(totalPoints):
        x = oneForthPoints[i][0]
        y = oneForthPoints[i][1]

        # Рисуем половину

        yHalfDown = y - 2 * abs(centerPoint[1] - y)
        canvas.create_line(x, yHalfDown, 
                           x + 1, yHalfDown + 1,
                           width = 1, fill = color)

        # Рисуем весь эллипс

        xFullDown = x - 2 * abs(centerPoint[0] - x) 
        canvas.create_line(xFullDown, y,
                           xFullDown + 1, y + 1,
                           width = 1, fill = color)
        canvas.create_line(xFullDown, yHalfDown,
                           xFullDown + 1, yHalfDown + 1,
                           width = 1, fill = color)

def parameterEquationEllipse(canvas, centerPoint, aAxisLength, bAxisLength, color):
    startAngle = math.pi / 2
    endAngle = 0
    step = 1 / bAxisLength
    x = centerPoint[0]
    y = centerPoint[1] + bAxisLength

    for i in numpy.arange(startAngle, endAngle - step, -step):
        # Рисуем 1/4

        canvas.create_line(x, y, 
                           x + 1, y + 1,
                           width = 1, fill = color)
        x = centerPoint[0] + aAxisLength * math.cos(i)
        y = centerPoint[1] + bAxisLength * math.sin(i)
        step = 1 / math.sqrt(x * x + y * y)

        # Рисуем половину

        yHalfDown = y - 2 * abs(centerPoint[1] - y)
        canvas.create_line(x, yHalfDown, 
                           x + 1, yHalfDown + 1,
                           width = 1, fill = color)

        # Рисуем весь эллипс

        xFullDown = x - 2 * abs(centerPoint[0] - x) 
        canvas.create_line(xFullDown, y,
                           xFullDown + 1, y + 1,
                           width = 1, fill = color)
        canvas.create_line(xFullDown, yHalfDown,
                           xFullDown + 1, yHalfDown + 1,
                           width = 1, fill = color)

def horStepEllipse(x, delta, bAxisSquared, doubleBAxisSquared):
    return x + 1, delta + doubleBAxisSquared * x + 3 * bAxisSquared

def diagStepEllipse(x, y, delta, 
                    aAxisSquared, doubleAAxisSquared,
                    bAxisSquared, doubleBAxisSquared):
    return x + 1, y - 1, delta + doubleBAxisSquared * x + 3 * bAxisSquared -\
                         doubleAAxisSquared * y + 3 * aAxisSquared

def vertStepEllipse(y, delta, aAxisSquared, doubleAAxisSquared):
    return y - 1, delta - doubleAAxisSquared * y + 3 * aAxisSquared

def brezenhamEllipse(canvas, centerPoint, aAxisLength, bAxisLength, color):
    x = 0
    y = bAxisLength

    aSquared = aAxisLength * aAxisLength
    bSquared = bAxisLength * bAxisLength
    doubleASquared = aSquared * 2
    doubleBSquared = bSquared * 2

    delta = bSquared - doubleASquared * bAxisLength + aSquared

    while (y >= 0):
        # Рисуем 1/4

        drawX = x + centerPoint[0]
        drawY = y + centerPoint[1]

        canvas.create_line(drawX, drawY,
                           drawX + 1, drawY + 1,
                           width = 1, fill = color)

        # Рисуем половину

        yHalfDown = drawY - 2 * abs(centerPoint[1] - drawY)
        canvas.create_line(drawX, yHalfDown,
                           drawX + 1, yHalfDown + 1,
                           width = 1, fill = color)

        # Рисуем весь эллипс

        xFullDown = drawX - 2 * abs(centerPoint[0] - drawX) 
        canvas.create_line(xFullDown, drawY,
                           xFullDown + 1, drawY + 1,
                           width = 1, fill = color)
        canvas.create_line(xFullDown, yHalfDown,
                           xFullDown + 1, yHalfDown + 1,
                           width = 1, fill = color)

        if (delta < 0):
            delta1 = 2 * (delta + y * aSquared) - aSquared

            if (delta1 <= 0):
                x, delta = horStepEllipse(x, delta, bSquared, doubleBSquared)
            else:
                x, y, delta = diagStepEllipse(x, y, delta, 
                                              aSquared, doubleASquared,
                                              bSquared, doubleBSquared)

        elif (delta > 0):
            delta2 = 2 * (delta - x * bSquared) - bSquared

            if (delta2 >= 0):
                y, delta = vertStepEllipse(y, delta, aSquared, doubleASquared)
            else:
                x, y, delta = diagStepEllipse(x, y, delta,
                                              aSquared, doubleASquared, 
                                              bSquared, doubleBSquared)

        else:
            x, y, delta = diagStepEllipse(x, y, delta,
                                          aSquared, doubleASquared, 
                                          bSquared, doubleBSquared)
        
def midPointEllipse(canvas, centerPoint, aAxisLength, bAxisLength, color):
    oneForthPoints = []

    aSquared = aAxisLength * aAxisLength
    bSquared = bAxisLength * bAxisLength

    doubleASquared = 2 * aSquared
    doubleBSquared = 2 * bSquared

    dx = 0
    dy = bAxisLength

    drawX = dx + centerPoint[0]
    drawY = dy + centerPoint[1]
        
    funcTry = bSquared - aSquared * bAxisLength + 0.25 * aSquared
    canvas.create_line(drawX, drawY,
                       drawX + 1, drawY + 1,
                       width = 1, fill = color)
    oneForthPoints.append([drawX, drawY])

    # Рисуем 1/4

    while (dx * bSquared <= dy * aSquared):

        if (funcTry < 0):
            dx += 1
        else:
            funcTry -= doubleASquared * dy
            dx += 1
            dy -= 1

        drawX = dx + centerPoint[0]
        drawY = dy + centerPoint[1]

        canvas.create_line(drawX, drawY, 
                           drawX + 1, drawY + 1,
                           width = 1, fill = color)
        oneForthPoints.append([drawX, drawY])

        funcTry += doubleBSquared * dx + bSquared


    funcTry += 0.75 * (aSquared - bSquared) - (bSquared * dx + aSquared * dy) 

    while (dy >= 0):

        if (funcTry > 0):
            dy -= 1
        else:
            funcTry += doubleBSquared * dx
            dx += 1
            dy -= 1

        drawX = dx + centerPoint[0]
        drawY = dy + centerPoint[1]

        canvas.create_line(drawX, drawY, 
                           drawX + 1, drawY + 1,
                           width = 1, fill = color)
        oneForthPoints.append([drawX, drawY])

        funcTry += (-doubleASquared * dy) + aSquared

    totalPoints = len(oneForthPoints)

    for i in range(totalPoints):
        x = oneForthPoints[i][0]
        y = oneForthPoints[i][1]

        # Рисуем половину

        yHalfDown = y - 2 * abs(centerPoint[1] - y)
        canvas.create_line(x, yHalfDown,
                           x + 1, yHalfDown + 1,
                           width = 1, fill = color)

        # Рисуем весь эллипс

        xFullDown = x - 2 * abs(centerPoint[0] - x) 
        canvas.create_line(xFullDown, y,
                           xFullDown + 1, y + 1,
                           width = 1, fill = color)
        canvas.create_line(xFullDown, yHalfDown,
                           xFullDown + 1, yHalfDown + 1,
                           width = 1, fill = color)

def tkinterAlgorithmEllipse(canvas, centerPoint, aAxisLength, bAxisLength, color):
    canvas.create_oval(centerPoint[0] - aAxisLength, centerPoint[1] - bAxisLength,
                       centerPoint[0] + aAxisLength, centerPoint[1] + bAxisLength,
                       width = 1, outline = color)

def drawSingleEllipse(canvas, algorithmType, color, centerPoint, aAxisLength, bAxisLength):
    if algorithmType == "Канон. ур-ие": 
        canonicEquationEllipse(canvas, centerPoint, aAxisLength, bAxisLength, color)
    elif algorithmType == "Парам. ур-ие":
        parameterEquationEllipse(canvas, centerPoint, aAxisLength, bAxisLength, color)
    elif algorithmType == "Брезенхем":
        brezenhamEllipse(canvas, centerPoint, aAxisLength, bAxisLength, color)
    elif algorithmType == "Средняя точка":
        midPointEllipse(canvas, centerPoint, aAxisLength, bAxisLength, color)
    elif algorithmType == "Библ. алг-м.":
        tkinterAlgorithmEllipse(canvas, centerPoint, aAxisLength, bAxisLength, color)

def drawVisualParamsCircle(canvas, algorithmType, color, centerPoint, r, rDelta, figuresNumber):
    timing = 0

    if algorithmType == "Канон. ур-ие":
        start = time.time()

        for i in range(figuresNumber):
            canonicEquationCircle(canvas, centerPoint, r, color)
            r += rDelta
        
        end = time.time()
        timing = (end - start) / figuresNumber 
    elif algorithmType == "Парам. ур-ие":
        start = time.time()

        for i in range(figuresNumber):
            parameterEquationCircle(canvas, centerPoint, r, color)
            r += rDelta

        end = time.time()
        timing = (end - start) / figuresNumber
    elif algorithmType == "Брезенхем":
        start = time.time()

        for i in range(figuresNumber):
            brezenhamCircle(canvas, centerPoint, r, color)
            r += rDelta

        end = time.time()
        timing = (end - start) / figuresNumber
    elif algorithmType == "Средняя точка":
        start = time.time()

        for i in range(figuresNumber):
            midPointCircle(canvas, centerPoint, r, color)
            r += rDelta
            
        end = time.time()
        timing = (end - start) / figuresNumber
    elif algorithmType == "Библ. алг-м.":
        start = time.time()

        for i in range(figuresNumber):
            tkinterAlgorithmCircle(canvas, centerPoint, r, color)
            r += rDelta

        end = time.time()
        timing = (end - start) / figuresNumber

    return timing

def drawVisualParamsEllipse(canvas, algorithmType, color, centerPoint,
                            aAxis, bAxis, aDelta, bDelta, figuresNumber):
    timing = 0

    if algorithmType == "Канон. ур-ие":
        start = time.time()

        for i in range(figuresNumber):
            canonicEquationEllipse(canvas, centerPoint, aAxis, bAxis, color)
            aAxis += aDelta
            bAxis += bDelta
        
        end = time.time()
        timing = (end - start) / figuresNumber 
    elif algorithmType == "Парам. ур-ие":
        start = time.time()

        for i in range(figuresNumber):
            parameterEquationEllipse(canvas, centerPoint, aAxis, bAxis, color)
            aAxis += aDelta
            bAxis += bDelta

        end = time.time()
        timing = (end - start) / figuresNumber
    elif algorithmType == "Брезенхем":
        start = time.time()

        for i in range(figuresNumber):
            brezenhamEllipse(canvas, centerPoint, aAxis, bAxis, color)
            aAxis += aDelta
            bAxis += bDelta

        end = time.time()
        timing = (end - start) / figuresNumber
    elif algorithmType == "Средняя точка":
        start = time.time()

        for i in range(figuresNumber):
            midPointEllipse(canvas, centerPoint, aAxis, bAxis, color)
            aAxis += aDelta
            bAxis += bDelta
            
        end = time.time()
        timing = (end - start) / figuresNumber
    elif algorithmType == "Библ. алг-м.":
        start = time.time()

        for i in range(figuresNumber):
            tkinterAlgorithmEllipse(canvas, centerPoint, aAxis, bAxis, color)
            aAxis += aDelta
            bAxis += bDelta

        end = time.time()
        timing = (end - start) / figuresNumber

    return timing

def getTimingCircle(canvas, algorithmList, color, centerPoint, r, rDelta, figuresNumber):
    timing  = dict()

    for i in range(len(algorithmList)):
        timing[algorithmList[i]] = drawVisualParamsCircle(canvas, algorithmList[i], color, centerPoint,
                                                          r, rDelta, figuresNumber)
    
    return timing

def getTimingEllipse(canvas, algorithmList, color, centerPoint,\
                     aAxis, bAxis, aDelta, bDelta, figuresNumber):
    timing  = dict()

    for i in range(len(algorithmList)):
        timing[algorithmList[i]] = drawVisualParamsEllipse(canvas, algorithmList[i], color, centerPoint,
                                                          aAxis, bAxis, aDelta, bDelta, figuresNumber)
    
    return timing
