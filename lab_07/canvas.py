from tkinter import*
import time
import numpy

class MyCanvas:

    def __init__(self):
        self.mainWindow = Tk()
        self.mainWindow.geometry("1200x1000+700+25")
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Отрисовка")

        self.widthCanvas = 1200
        self.heightCanvas = 1000
        
        self.lineColor = "#000000"
        self.cutterColor = "#000000"
        self.cutLineColor = "#0404d9"
        self.backgroundColor = "#ffffff"

        self.lineColorRGB = (0, 0, 0)
        self.cutterColorRGB = (0, 0, 0)
        self.cutLineColorRGB = (4, 4, 217)

        self.lines = []
        self.cutter = []

        self.lineInputFlag = False
        self.cutterInputFlag = True

        self.canvas = Canvas(self.mainWindow, width = self.widthCanvas, height = self.heightCanvas, bg = "white")
        self.canvas.place(x = 0, y = 0)

        self.mainImage = PhotoImage(height = self.heightCanvas, width = self.widthCanvas)
        self.canvas.create_image((self.widthCanvas // 2, self.heightCanvas // 2), image = self.mainImage, state = "normal")

        self.fillBackground()

    def brezenhamInt(self, canvas, pointStart, pointEnd, lineColour):
        change = 0

        pointStart = [round(pointStart[0]), round(pointStart[1])]
        pointEnd = [round(pointEnd[0]), round(pointEnd[1])]

        dx = pointEnd[0] - pointStart[0]
        dy = pointEnd[1] - pointStart[1]

        signX = numpy.sign(dx)
        signY = numpy.sign(dy)

        dx = abs(dx)
        dy = abs(dy)

        if (dx > dy):
            change = 0
        else:
            temp = dx
            dx = dy
            dy = temp
            change = 1
        
        error = 2 * dy - dx # Отличие от вещественного алгоритма
        deltaErrorX = 2 * dx
        deltaErrorY = 2 * dy 
        
        x = round(pointStart[0])
        y = round(pointStart[1])

        for i in range(0, dx + 1):
            canvas.put(lineColour, to = (x, y))

            if (error >= 0):
                if (change == 1):
                    x += signX
                else:
                    y += signY

                error -= deltaErrorX
        
            if (change == 0):
                x += signX
            else:
                y += signY
                
            error += deltaErrorY

    def fillBackground(self):
        self.mainImage.put(self.backgroundColor, to = (0, 0, self.widthCanvas, self.heightCanvas))

    def addLinePoint(self, event):
        newPoint = [event.x, event.y]
        self.lines.append(newPoint)

        if (not self.lineInputFlag):
            self.lineInputFlag = True
        else:
            size = len(self.lines)
            self.brezenhamInt(self.mainImage, self.lines[size - 1], self.lines[size - 2], self.lineColor)
            self.lineInputFlag = False

    def addStraightLinePoint(self, event):
        if (not self.lineInputFlag):
            newPoint = [event.x, event.y]
            self.lines.append(newPoint)
            self.lineInputFlag = True
        else:
            size = len(self.lines)
            lastPoint = self.lines[size - 1]
            newPoint = []

            if (abs(event.x - lastPoint[0]) > abs(event.y - lastPoint[1])):
                newPoint.append(event.x)
                newPoint.append(lastPoint[1])
            else:
                newPoint.append(lastPoint[0])
                newPoint.append(event.y)

            self.lines.append(newPoint)
            size += 1
            self.brezenhamInt(self.mainImage, self.lines[size - 1], self.lines[size - 2], self.lineColor)
            self.lineInputFlag = False
        
    def addCutterSide(self, event):
        if (self.cutterInputFlag):
            newPoint = [event.x, event.y]
            self.cutter.append(newPoint)
            self.cutterInputFlag = False
        else:
            size = len(self.cutter)
            lastPoint = self.cutter[size - 1]
            newPoint = []

            if (abs(event.x - lastPoint[0]) > abs(event.y - lastPoint[1])):
                newPoint.append(event.x)
                newPoint.append(lastPoint[1])
            else:
                newPoint.append(lastPoint[0])
                newPoint.append(event.y)

            self.cutter.append(newPoint)
            self.brezenhamInt(self.mainImage, self.cutter[size], self.cutter[size - 1], self.cutterColor)

    def finishCutter(self, event):
        lastCutterTop = []

        if (self.cutter[1][1] == self.cutter[0][1]):
            lastCutterTop.append(self.cutter[0][0])
        else:
            lastCutterTop.append(self.cutter[2][0])

        if (self.cutter[1][0] == self.cutter[0][0]):
            lastCutterTop.append(self.cutter[0][1])
        else:
            lastCutterTop.append(self.cutter[2][1])
        
        self.cutter.append(lastCutterTop)
        self.brezenhamInt(self.mainImage, self.cutter[2], self.cutter[3], self.cutterColor)
        self.brezenhamInt(self.mainImage, self.cutter[3], self.cutter[0], self.cutterColor)

    def getCodes(self, point, xL, xR, yD, yU):
        T = []

        if (point[0] < xL): 
            T.append(1)
        else:
            T.append(0)

        if (point[0] > xR):
            T.append(1)
        else:
            T.append(0)

        if (point[1] < yD):
            T.append(1)
        else:
            T.append(0)

        if (point[1] > yU):
            T.append(1)
        else:
            T.append(0)

        return T

    def getSum(self, T):
        sum = 0

        for i in range(len(T)):
            sum += T[i]

        return sum

    def calcScalarProduct(self, T1, T2):
        result = 0

        for i in range(len(T1)):
            result += T1[i] * T2[i]

        return result

    def checkIntersection(self, temp, m, xL, xR, yD, yU):
        R = []
        xResult = yResult = 0
        
        if m != 1e30:
            if (temp[0] < xL):
                yResult = m * (xL - temp[0]) + temp[1]
                
                if (yD <= yResult <= yU): # Проверка на корректность
                    R = [xL, yResult]

            if (temp[0] > xR):
                yResult = m * (xR - temp[0]) + temp[1]

                if (yD <= yResult <= yU):
                    R = [xR, yResult]
        
        if (temp[1] < yD):
            xResult = (1 / m) * (yD - temp[1]) + temp[0]

            if (xL <= xResult <= xR):
                R = [xResult, yD]

        if (temp[1] > yU):
            xResult = (1 / m) * (yU - temp[1]) + temp[0]

            if (xL <= xResult <= xR):
                R = [xResult, yU]

        return R

    def cutLine(self, start, end, xL, xR, yD, yU):
        T1 = self.getCodes(start, xL, xR, yD, yU)
        T2 = self.getCodes(end, xL, xR, yD, yU)

        S1 = self.getSum(T1)
        S2 = self.getSum(T2)

        flag = 1 # 1 - отрезок видимый, pr = -1 - отрезок невидимый
        m = 1e30 # Предполагаем, что отрезок вертикальный

        if (S1 == 0 and S2 == 0):
            self.brezenhamInt(self.mainImage, start, end, self.cutLineColor)
            return flag

        PL = self.calcScalarProduct(T1, T2) # Логическое произведение

        if (PL != 0): # Тривиальная невидимость
            flag = -1
            return flag

        R1 = []
        R2 = []
        temp = []

        if (start[0] != end[0]):
            m = (end[1] - start[1]) / (end[0] - start[0])  

        if (S1 == 0):
            R1 = start
            temp = end            

        if (S2 == 0):
            R1 = end
            temp = start

        if (S1 == 0 or S2 == 0):
            R2 = self.checkIntersection(temp, m, xL, xR, yD, yU)
            self.brezenhamInt(self.mainImage, R1, R2, self.cutLineColor)
            return flag

        temp = start
        R1 = self.checkIntersection(temp, m, xL, xR, yD, yU)

        if (R1 == []):
            flag = -1
            return flag

        temp = end
        R2 = self.checkIntersection(temp, m, xL, xR, yD, yU)
        self.brezenhamInt(self.mainImage, R1, R2, self.cutLineColor)

        return flag

    def getLimitsX(self, cutter):
        xMin = xMax = cutter[0][0]

        for i in range(len(cutter)):
            if cutter[i][0] < xMin:
                xMin = cutter[i][0]
            if cutter[i][0] > xMax:
                xMax = cutter[i][0]

        return xMin, xMax

    def getLimitsY(self, cutter):
        yMin = yMax = cutter[0][1]

        for i in range(len(cutter)):
            if cutter[i][1] < yMin:
                yMin = cutter[i][1]
            if cutter[i][1] > yMax:
                yMax = cutter[i][1]

        return yMin, yMax

    def cut(self):
        xLeft, xRight = self.getLimitsX(self.cutter)
        yDown, yUp = self.getLimitsY(self.cutter)
        self.refillArea(xLeft, xRight, yDown, yUp)

        for i in range(len(self.lines) // 2):
            self.cutLine(self.lines[i * 2], self.lines[i * 2 + 1], xLeft, xRight, yDown, yUp)

    def refillArea(self, xL, xR, yD, yU):
        self.mainImage.put(self.backgroundColor, to = (xL + 1, yD + 1, xR, yU))