from tkinter import*
import calculations
from checks import*
from tkinter.messagebox import*
import matplotlib.pyplot as plot
import numpy as np
import math

class Window:

    def __init__(self, canvasWnd):
        self.mainWindow = Tk()
        self.mainWindow.geometry('500x570+100+300')
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Лаборторная работа №3") 

        self.mainFrame = Frame(self.mainWindow, width = 500, height = 570, relief = RAISED, borderwidth = 3, bg = "black")
        self.mainFrame.place(x = 0, y = 0)

        # Построение одиночных отрезков

        self.singleLineFrame = Frame(self.mainFrame, width = 450, height = 170, relief = RAISED, borderwidth = 3, bg = "black")
        self.singleLineFrame.place(x = 25, y = 80)

        self.algorithmLabel = Label(self.mainFrame, text = "Алгоритм", fg = "white", bg = "black")
        self.algorithmLabel.place(x = 85, y = 10)

        self.colorLabel = Label(self.mainFrame, text = "Цвет", fg = "white", bg = "black")
        self.colorLabel.place(x = 380, y = 10)

        self.singleLineLabel = Label(self.mainFrame, text = "Одиночный отрезок", fg = "white", bg = "black")
        self.singleLineLabel.place(x = 180, y = 85)

        self.startPointLabel = Label(self.mainFrame, text = "Начальная точка", fg = "white", bg = "black")
        self.startPointLabel.place(x = 50, y = 110)

        self.endPointLabel = Label(self.mainFrame, text = "Конечная точка", fg = "white", bg = "black")
        self.endPointLabel.place(x = 320, y = 110)

        self.startPointLabelX = Label(self.mainFrame, text = "x", fg = "white", bg = "black")
        self.startPointLabelX.place(x = 60, y = 130)

        self.startPointLabelY = Label(self.mainFrame, text = "y", fg = "white", bg = "black")
        self.startPointLabelY.place(x = 150, y = 130)

        self.endPointLabelX = Label(self.mainFrame, text = "x", fg = "white", bg = "black")
        self.endPointLabelX.place(x = 330, y = 130)

        self.endPointLabelY = Label(self.mainFrame, text = "y", fg = "white", bg = "black")
        self.endPointLabelY.place(x = 420, y = 130)

        self.startPointEntryX = Entry(self.mainFrame, width = 6, bd = 1)
        self.startPointEntryX.place(x = 40, y = 150)

        self.startPointEntryY = Entry(self.mainFrame, width = 6, bd = 1)
        self.startPointEntryY.place(x = 130, y = 150)    

        self.endPointEntryX = Entry(self.mainFrame, width = 6, bd = 1)
        self.endPointEntryX.place(x = 310, y = 150)
        
        self.endPointEntryY = Entry(self.mainFrame, width = 6, bd = 1)
        self.endPointEntryY.place(x = 400, y = 150)

        self.algorithmList = ["Цифр. анализатор", "Брезенхем вещ.", "Брезенхем цел.", "Брезенхем устр. ступ.", "Ву", "Библ. алг-м."]
        self.algorithmVar = StringVar(self.mainFrame)
        self.algorithmVar.set(self.algorithmList[0])
        self.algorithmMenu = OptionMenu(self.mainFrame, self.algorithmVar, *self.algorithmList)
        self.algorithmMenu.config(width = 19, bg = "white", bd = 1, relief = RIDGE)
        self.algorithmMenu.place(x = 30, y = 40)

        self.colorList = ["Черный", "Белый", "Коричневый", "Зеленый", "Желтый", "Розовый"]
        self.colorDict = { "Черный" : "black", "Белый" : "white", "Коричневый" : "brown", "Зеленый" : "green", "Желтый": "yellow", "Розовый" : "pink"}
        self.colorVar = StringVar(self.mainFrame)
        self.colorVar.set(self.colorList[0])
        self.colorMenu = OptionMenu(self.mainFrame, self.colorVar, *self.colorList)
        self.colorMenu.config(width = 10, bg = "white", bd = 1, relief = RIDGE)
        self.colorMenu.place(x = 340, y = 40)

        self.drawLineButton = Button(self.mainFrame, text = "Нарисовать отрезок", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.drawSingle)
        self.drawLineButton.place(x = 150, y = 190)

        # Сравнение визуальных характеристик

        self.visualParamsFrame = Frame(self.mainFrame, width = 450, height = 170, relief = RAISED, borderwidth = 3, bg = "black")
        self.visualParamsFrame.place(x = 25, y = 265)

        self.visualParamsLabel = Label(self.mainFrame, text = "Сравнение визуальных параметров", fg = "white", bg = "black")
        self.visualParamsLabel.place(x = 130, y = 270)

        self.centralPointLabel = Label(self.mainFrame, text = "Центральная точка", fg = "white", bg = "black")

        self.centralPointLabel.place(x = 45, y = 290)
        self.centralPointLabelX = Label(self.mainFrame, text = "x", fg = "white", bg = "black")
        self.centralPointLabelX.place(x = 60, y = 310)

        self.centralPointLabelX = Label(self.mainFrame, text = "y", fg = "white", bg = "black")
        self.centralPointLabelX.place(x = 150, y = 310)

        self.lengthLabel = Label(self.mainFrame, text = "Длина отрезков", fg = "white", bg = "black")
        self.lengthLabel.place(x = 220, y = 290)

        self.deltaAngleLabel = Label(self.mainFrame, text = "Шаг угла", fg = "white", bg = "black")
        self.deltaAngleLabel.place(x = 380, y = 290)

        self.centralPointEntryX = Entry(self.mainFrame, width = 6, bd = 1)
        self.centralPointEntryX.place(x = 40, y = 330)

        self.centralPointEntryY = Entry(self.mainFrame, width = 6, bd = 1)
        self.centralPointEntryY.place(x = 130, y = 330)

        self.lengthEntry = Entry(self.mainFrame, width = 6, bd = 1)
        self.lengthEntry.place(x = 250, y = 330)

        self.deltaAngleEntry = Entry(self.mainFrame, width = 6, bd = 1)
        self.deltaAngleEntry.place(x = 387, y = 330)

        self.drawVisualParamsButton = Button(self.mainFrame, text = "Показать", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.drawVisualParams)
        self.drawVisualParamsButton.place(x = 150, y = 370)

        # Исследование временных характеристик

        self.timingParamsButton = Button(self.mainFrame, text = "Временные\nзатраты", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.getTiming)
        self.timingParamsButton.place(x = 25, y = 450)

        # Исследование ступенчатости

        self.steppingParamsButton = Button(self.mainFrame, text = "Исследование\nступенчатости", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.getStepping)
        self.steppingParamsButton.place(x = 285, y = 450)

        # Кнопка очистки

        self.cleanButton = Button(self.mainFrame, text = "Очистка", width = 53, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.clear)
        self.cleanButton.place(x = 23, y = 508)

        self.canvasHandler = canvasWnd.canvas

    def drawSingle(self):
        xStart = self.startPointEntryX.get()
        yStart = self.startPointEntryY.get()
        xEnd = self.endPointEntryX.get()
        yEnd = self.endPointEntryY.get()
        
        if (not is_float(xStart) or not is_float(yStart) or not is_float(xStart) or not is_float(yStart)):
            showwarning("Предупреждение", "Введите целые или вещественные числа!")
        else:
            calculations.drawSingle(self.canvasHandler, self.algorithmVar.get(), self.colorDict[self.colorVar.get()],
                                    [round(float(xStart)), round(float(yStart))], [round(float(xEnd)), round(float(yEnd))])

    def drawVisualParams(self):
        xCenter = self.centralPointEntryX.get()
        yCenter = self.centralPointEntryY.get()
        length = self.lengthEntry.get()
        deltaAngle = self.deltaAngleEntry.get()

        if (not is_float(xCenter) or not is_float(yCenter) or not is_float(length) or not is_float(deltaAngle)):
            showwarning("Предупреждение", "Введите целые или вещественные числа!")
        else:
            calculations.drawVisualParams(self.canvasHandler, self.algorithmVar.get(), self.colorDict[self.colorVar.get()],
                                          [round(float(xCenter)), round(float(yCenter))], round(float(length)), round(float(deltaAngle)))

    def showTiming(self, timeInfo):
        if (len(timeInfo) > 0):
            x = np.arange(len(timeInfo))

            colors = np.random.rand(len(timeInfo), 3)
            plot.bar(x, height = timeInfo.values(), color = colors)
            plot.xticks(x, timeInfo.keys())
            plot.xlabel("Алгоритм")
            plot.ylabel("Время(c)")
            plot.title("Исследование затраченного времени")
            plot.show()

    def getTiming(self):
        xCenter = self.centralPointEntryX.get()
        yCenter = self.centralPointEntryY.get()
        length = self.lengthEntry.get()
        deltaAngle = self.deltaAngleEntry.get()
        timing = dict()

        if (not is_float(xCenter) or not is_float(yCenter) or not is_float(length) or not is_float(deltaAngle)):
            showwarning("Предупреждение", "Введите целые или вещественные числа!")
        else:
            timing = calculations.getTiming(self.canvasHandler, self.algorithmList, self.colorDict[self.colorVar.get()],
                                            [round(float(xCenter)), round(float(yCenter))], round(float(length)), round(float(deltaAngle)))

        self.showTiming(timing)

    def getStepping(self):
        xCenter = self.centralPointEntryX.get()
        yCenter = self.centralPointEntryY.get()
        length = self.lengthEntry.get()
        deltaAngle = 1

        if (not is_float(xCenter) or not is_float(yCenter) or not is_float(length)):
            showwarning("Предупреждение", "Введите целые или вещественные числа!")
        else:
            stepsNumber = calculations.getSteps(self.algorithmVar.get(), [round(float(xCenter)), round(float(yCenter))],
                                                round(float(length)), round(float(deltaAngle)))
            
            x = [(i / 180) * math.pi for i in range(0, 360, round(deltaAngle))]

            if (len(stepsNumber) == len(x)):
                plot.title("Иccледование ступенчатости(ЦДА): длина отрезка - " + str(length))
                plot.xlabel("Угол(радиан)")
                plot.ylabel("Количество ступенек")
                plot.plot(x, stepsNumber, color = 'm')
                plot.show()

    def clear(self):
        self.canvasHandler.delete("all")

    def loop(self):
        self.mainWindow.mainloop()