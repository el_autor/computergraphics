from tkinter import*
import numpy
import math
import time

def diffAnalyzerLine(canvas, pointStart, pointEnd, lineColour):
    l = 0
    dx = pointEnd[0] - pointStart[0]
    dy = pointEnd[1] - pointStart[1]

    absX = abs(dx)
    absY = abs(dy)

    if (absX > absY):
        l = absX
    else:
        l = absY

    dx = dx / float(l)
    dy = dy / float(l)

    x = pointStart[0]
    y = pointStart[1]

    for i in numpy.arange(0, l, 1):
        canvas.create_line(round(x), round(y), round(x) + 1, round(y) + 1,
                           width = 1, fill = lineColour)
        x += dx
        y += dy

def diffAnalyzerStepping(pointStart, pointEnd):
    totalSteps = 0
    l = 0
    dx = pointEnd[0] - pointStart[0]
    dy = pointEnd[1] - pointStart[1]

    absX = abs(dx)
    absY = abs(dy)

    if (absX > absY):
        l = absX
    else:
        l = absY

    dx = dx / float(l)
    dy = dy / float(l)

    x = pointStart[0]
    y = pointStart[1]

    for i in numpy.arange(0, l, 1):

        if (abs(round(x + dx) - round(x)) == 1 and abs(round(y + dy) - round(y)) == 1):
            totalSteps += 1
        
        x += dx
        y += dy

    return totalSteps

def brezenhamFloat(canvas, pointStart, pointEnd, lineColour):
    change = 0

    dx = pointEnd[0] - pointStart[0]
    dy = pointEnd[1] - pointStart[1]

    signX = numpy.sign(dx)
    signY = numpy.sign(dy)

    dx = abs(dx)
    dy = abs(dy)

    if (dx > dy):
        change = 0
    else:
        temp = dx
        dx = dy
        dy = temp
        change = 1
    
    derivative = dy / dx
    error = derivative - 1 / 2 # Начальное значение ошибки

    x = round(pointStart[0])
    y = round(pointStart[1])

    for i in numpy.arange(0, dx, 1):
        canvas.create_line(x, y, x + 1, y + 1, width = 1, fill = lineColour)

        if (error >= 0):
            if (change == 1):
                x += signX
            else:
                y += signY

            error -= 1
    
        if (change == 0):
            x += signX
        else:
            y += signY
            
        error += derivative
    
def brezenhamInt(canvas, pointStart, pointEnd, lineColour):
    change = 0

    dx = pointEnd[0] - pointStart[0]
    dy = pointEnd[1] - pointStart[1]

    signX = numpy.sign(dx)
    signY = numpy.sign(dy)

    dx = abs(dx)
    dy = abs(dy)

    if (dx > dy):
        change = 0
    else:
        temp = dx
        dx = dy
        dy = temp
        change = 1
    
    error = 2 * dy - dx # Отличие от вещественного алгоритма
    deltaErrorX = 2 * dx
    deltaErrorY = 2 * dy 
    
    x = round(pointStart[0])
    y = round(pointStart[1])

    for i in numpy.arange(0, dx, 1):
        canvas.create_line(x, y, x + 1, y + 1, width = 1, fill = lineColour)

        if (error >= 0):
            if (change == 1):
                x += signX
            else:
                y += signY

            error -= deltaErrorX
    
        if (change == 0):
            x += signX
        else:
            y += signY
            
        error += deltaErrorY
 
def brezenhamNoStepping(canvas, pointStart, pointEnd):
    colors = ["#ffffff", "#cdcdcd", "#9a9a9a", "#666666", "#303030", "#000000"]
    I = 5

    change = 0

    dx = pointEnd[0] - pointStart[0]
    dy = pointEnd[1] - pointStart[1]

    signX = numpy.sign(dx)
    signY = numpy.sign(dy)

    dx = abs(dx)
    dy = abs(dy)

    if (dx > dy):
        change = 0
    else:
        temp = dx
        dx = dy
        dy = temp
        change = 1
    
    derivative = I * (dy / float(dx))
    error = I / 2.0
    weight = I - derivative

    x = round(pointStart[0])
    y = round(pointStart[1])
    canvas.create_line(x, y, x + 1, y + 1, width = 1, fill = colors[round(error)])

    for i in numpy.arange(0, dx, 1):
        if (error < weight):
            if (change == 1):
                y += signY
            else:
                x += signX

            error += derivative
        else:
            x += signX
            y += signY
            error -= weight 

        canvas.create_line(x, y, x + 1, y + 1, width = 1, fill = colors[round(error)])

def drawPointWu(canvas, swapped, x, y, brightness, colors):
    index = round(5 * brightness)

    if (not swapped):
        canvas.create_line(x, y, x + 1, y + 1, width = 1, fill = colors[index])
    else:
        canvas.create_line(y, x, y + 1, x + 1, width = 1, fill = colors[index])

def wu(canvas, pointStart, pointEnd):
    colors = ["#ffffff", "#cdcdcd", "#9a9a9a", "#666666", "#303030", "#000000"]

    pointStartX = pointStart[0]
    pointStartY = pointStart[1]
    pointEndX = pointEnd[0]
    pointEndY = pointEnd[1]
    
    swapped = (abs(pointEndY - pointStartY) > abs(pointEndX - pointStartX))

    if (swapped):
        pointStartX, pointStartY = pointStartY, pointStartX
        pointEndX, pointEndY = pointEndY, pointEndX

    if (pointStartX > pointEndX):
        pointStartX, pointEndX = pointEndX, pointStartX
        pointStartY, pointEndY = pointEndY, pointStartY

    dx = pointEndX - pointStartX
    dy = pointEndY - pointStartY
    derivative = dy / float(dx)

    x = pointStartX 
    y = pointStartY + derivative

    while (x <= pointEndX):
        brightness = y - int(y)
        drawPointWu(canvas, swapped, x, int(y), 1 - brightness, colors) # 1 линия
        drawPointWu(canvas, swapped, x, int(y) + 1, brightness, colors) # 2 линия
        x += 1
        y += derivative


def drawSingle(canvas, algorithmType, lineColour, pointStart, pointEnd):
    if algorithmType == "Библ. алг-м.":
        canvas.create_line(pointStart[0], pointStart[1], pointEnd[0], pointEnd[1], width = 1, fill = lineColour)
    elif algorithmType == "Цифр. анализатор":
        diffAnalyzerLine(canvas, pointStart, pointEnd, lineColour)
    elif algorithmType == "Брезенхем вещ.":
        brezenhamFloat(canvas, pointStart, pointEnd, lineColour)
    elif algorithmType == "Брезенхем цел.":
        brezenhamInt(canvas, pointStart, pointEnd, lineColour)
    elif algorithmType == "Брезенхем устр. ступ.":
        brezenhamNoStepping(canvas, pointStart, pointEnd)
    elif algorithmType == "Ву":
        wu(canvas, pointStart, pointEnd)

def getLines(centerPoint, rad, deltaAngle):
    lines = []

    for i in numpy.arange(0, math.pi * 2, deltaAngle):
        line = []

        pointXStart = rad * math.cos(i) + centerPoint[0] 
        pointYStart = rad * math.sin(i) + centerPoint[1]
        line.append([round(pointXStart), round(pointYStart)])

        pointXEnd = rad * math.cos(i + math.pi) + centerPoint[0]
        pointYEnd = rad * math.sin(i + math.pi) + centerPoint[1]
        line.append([round(pointXEnd), round(pointYEnd)])

        lines.append(line)

    return lines

def drawVisualParams(canvas, algorithmType, lineColour, centerPoint, length, deltaAngle):
    linesToDraw = getLines(centerPoint, length / 2.0, (deltaAngle / 180) * math.pi)
    linesToDraw = linesToDraw[:180]
    timing = 0

    if algorithmType == "Библ. алг-м.":
        start = time.time()

        for i in range(len(linesToDraw)):
            canvas.create_line(linesToDraw[i][0][0], linesToDraw[i][0][1], linesToDraw[i][1][0], linesToDraw[i][1][1], width = 1, fill = lineColour) 

        end = time.time()
        timing = (end - start) / float(len(linesToDraw))
    elif algorithmType == "Цифр. анализатор":
        start = time.time()

        for i in range(len(linesToDraw)):
            diffAnalyzerLine(canvas, linesToDraw[i][0], linesToDraw[i][1], lineColour)
        
        end = time.time()
        timing = (end - start) / float(len(linesToDraw))
    elif algorithmType == "Брезенхем вещ.":
        start = time.time()

        for i in range(len(linesToDraw)):
            brezenhamFloat(canvas, linesToDraw[i][0], linesToDraw[i][1], lineColour)    

        end = time.time()
        timing = (end - start) / float(len(linesToDraw))
    elif algorithmType == "Брезенхем цел.":
        start = time.time()

        for i in range(len(linesToDraw)):
            brezenhamInt(canvas, linesToDraw[i][0], linesToDraw[i][1], lineColour)  

        end = time.time()
        timing = (end - start) / float(len(linesToDraw))     
    elif algorithmType == "Брезенхем устр. ступ.":
        start = time.time()

        for i in range(len(linesToDraw)):
            brezenhamNoStepping(canvas, linesToDraw[i][0], linesToDraw[i][1])                
    
        end = time.time()
        timing = (end - start) / float(len(linesToDraw))
    elif algorithmType == "Ву":
        start = time.time()

        for i in range(len(linesToDraw)):
            wu(canvas, linesToDraw[i][0], linesToDraw[i][1])

        end = time.time()
        timing = (end - start) / float(len(linesToDraw))
    
    return timing

def getTiming(canvas, algorithmList, lineColour, centerPoint, length, angle):
    timing  = dict()

    for i in range(len(algorithmList)):
        if algorithmList[i] == "Библ. алг-м.":
            timing["Библ. алг-м."] = drawVisualParams(canvas, "Библ. алг-м.", lineColour, centerPoint, length, angle)
        elif algorithmList[i] == "Цифр. анализатор":
            timing["Цифр. анализатор"] = drawVisualParams(canvas, "Цифр. анализатор", lineColour, centerPoint, length, angle)
        elif algorithmList[i] == "Брезенхем вещ.":
            timing["Брезенхем вещ."] = drawVisualParams(canvas, "Брезенхем вещ.", lineColour, centerPoint, length, angle)
        elif algorithmList[i] == "Брезенхем цел.":
            timing["Брезенхем цел."] = drawVisualParams(canvas, "Брезенхем цел.", lineColour, centerPoint, length, angle)
        elif algorithmList[i] == "Брезенхем устр. ступ.":
            timing["Брезенхем устр. ступ."] = drawVisualParams(canvas, "Брезенхем устр. ступ.", lineColour, centerPoint, length, angle)
        elif algorithmList[i] == "Ву":
            timing["Ву"] = drawVisualParams(canvas, "Ву", lineColour, centerPoint, length, angle)

    return timing

def getSteps(algorithmType, centerPoint, length, deltaAngle):
    linesToDraw = getLines(centerPoint, length / 2.0, (deltaAngle / 180) * math.pi)
    steppingCountList = []

    deltaAngle = round(deltaAngle)

    if algorithmType == "Цифр. анализатор":
         for i in range(0, len(linesToDraw), deltaAngle):
             steppingCountList.append(diffAnalyzerStepping(linesToDraw[i][0], linesToDraw[i][1]))

    return steppingCountList