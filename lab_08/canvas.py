from tkinter import*
from tkinter.messagebox import*
import time
import numpy

class MyCanvas:

    def __init__(self):
        self.mainWindow = Tk()
        self.mainWindow.geometry("1200x1000+700+25")
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Отрисовка")

        self.widthCanvas = 1200
        self.heightCanvas = 1000
        
        self.lineColor = "#d90110"
        self.cutterColor = "#100bd9"
        self.cutLineColor = "#28d910"
        self.backgroundColor = "#ffffff"

        self.lineColorRGB = (0, 0, 0)
        self.cutterColorRGB = (0, 0, 0)
        self.cutLineColorRGB = (4, 4, 217)

        self.lines = []
        self.cutter = []

        self.lineInputFlag = False
        self.cutterInputFlag = True

        self.canvas = Canvas(self.mainWindow, width = self.widthCanvas, height = self.heightCanvas, bg = "white")
        self.canvas.place(x = 0, y = 0)

        self.mainImage = PhotoImage(height = self.heightCanvas, width = self.widthCanvas)
        self.canvas.create_image((self.widthCanvas // 2, self.heightCanvas // 2), image = self.mainImage, state = "normal")

        self.fillBackground()

    def brezenhamInt(self, canvas, pointStart, pointEnd, lineColour):
        change = 0

        pointStart = [round(pointStart[0]), round(pointStart[1])]
        pointEnd = [round(pointEnd[0]), round(pointEnd[1])]

        dx = pointEnd[0] - pointStart[0]
        dy = pointEnd[1] - pointStart[1]

        signX = numpy.sign(dx)
        signY = numpy.sign(dy)

        dx = abs(dx)
        dy = abs(dy)

        if (dx > dy):
            change = 0
        else:
            temp = dx
            dx = dy
            dy = temp
            change = 1
        
        error = 2 * dy - dx # Отличие от вещественного алгоритма
        deltaErrorX = 2 * dx
        deltaErrorY = 2 * dy 
        
        x = round(pointStart[0])
        y = round(pointStart[1])

        for i in range(0, dx + 1):
            canvas.put(lineColour, to = (x, y))

            if (error >= 0):
                if (change == 1):
                    x += signX
                else:
                    y += signY

                error -= deltaErrorX
        
            if (change == 0):
                x += signX
            else:
                y += signY
                
            error += deltaErrorY

    def fillBackground(self):
        self.mainImage.put(self.backgroundColor, to = (0, 0, self.widthCanvas, self.heightCanvas))

    def addLinePoint(self, event):
        newPoint = [event.x, event.y]
        self.lines.append(newPoint)

        if (not self.lineInputFlag):
            self.lineInputFlag = True
        else:
            size = len(self.lines)
            self.brezenhamInt(self.mainImage, self.lines[size - 1], self.lines[size - 2], self.lineColor)
            self.lineInputFlag = False

    def addStraightLinePoint(self, event):
        if (not self.lineInputFlag):
            newPoint = [event.x, event.y]
            self.lines.append(newPoint)
            self.lineInputFlag = True
        else:
            size = len(self.lines)
            lastPoint = self.lines[size - 1]
            newPoint = []

            if (abs(event.x - lastPoint[0]) > abs(event.y - lastPoint[1])):
                newPoint.append(event.x)
                newPoint.append(lastPoint[1])
            else:
                newPoint.append(lastPoint[0])
                newPoint.append(event.y)

            self.lines.append(newPoint)
            size += 1
            self.brezenhamInt(self.mainImage, self.lines[size - 1], self.lines[size - 2], self.lineColor)
            self.lineInputFlag = False
        
    def addParallelLinePoint(self, event):
        if (not self.lineInputFlag):
            newPoint = [event.x, event.y]
            self.lines.append(newPoint)
            self.lineInputFlag = True
        else:
            size = len(self.lines)
            lastPoint = self.lines[size - 1]
            curK = (event.y - lastPoint[1]) / (event.x - lastPoint[0])

            if (len(self.cutter) < 2):
                self.lines.append([event.x, event.y])
                self.brezenhamInt(self.mainImage, self.lines[size], self.lines[size - 1], self.lineColor)    
            else:
                closestK = (self.cutter[1][1] - self.cutter[0][1]) / (self.cutter[1][0] - self.cutter[0][0])

                for i in range(len(self.cutter) - 1):
                    tempK = (self.cutter[i + 1][1] - self.cutter[i][1]) / (self.cutter[i + 1][0] - self.cutter[i][0])

                    if (abs(curK - closestK) > abs(curK - tempK)):
                        closestK = tempK
                
                newPoint = [event.x, lastPoint[1] + (event.x - lastPoint[0]) * closestK]
                self.lines.append(newPoint)

                self.brezenhamInt(self.mainImage, self.lines[size], self.lines[size - 1], self.lineColor)

            self.lineInputFlag = False

    def addCutterSide(self, event):
        newPoint = [event.x, event.y]
        self.cutter.append(newPoint)

        if (self.cutterInputFlag):
            self.cutterInputFlag = False
        else:
            size = len(self.cutter)
            self.brezenhamInt(self.mainImage, self.cutter[size - 1], self.cutter[size - 2], self.cutterColor)

    def addStraightCutterSide(self, event):
        if (self.cutterInputFlag):
            newPoint = [event.x, event.y]
            self.cutter.append(newPoint)
            self.lineInputFlag = False
        else:
            size = len(self.cutter)
            lastPoint = self.cutter[size - 1]
            newPoint = []

            if (abs(event.x - lastPoint[0]) > abs(event.y - lastPoint[1])):
                newPoint.append(event.x)
                newPoint.append(lastPoint[1]) 
            else:
                newPoint.append(lastPoint[0])
                newPoint.append(event.y)

            self.cutter.append(newPoint)
            self.brezenhamInt(self.mainImage, self.cutter[size], self.cutter[size - 1], self.lineColor)

    def finishCutter(self, event):
        self.brezenhamInt(self.mainImage, self.cutter[len(self.cutter) - 1], self.cutter[0], self.cutterColor)

    def polygonIsConvex(self, polygonTops):
        plusOne = False
        minusOne = False
        zeroOne = False
        polygonTops.append(polygonTops[0])
        polygonTops.append(polygonTops[1])
        size = len(polygonTops)
       
        for i in range(0, size - 2):
            v1 = [polygonTops[i + 1][0] - polygonTops[i][0], polygonTops[i + 1][1] - polygonTops[i][1]]
            v2 = [polygonTops[i + 2][0] - polygonTops[i + 1][0], polygonTops[i + 2][1] - polygonTops[i + 1][1]]
            sign = numpy.sign(v1[0] * v2[1] - v1[1] * v2[0])
            
            if sign == 0:
                zeroOne = True
            elif sign < 0:
                minusOne = True
            else: 
                plusOne = True

        polygonTops = polygonTops[:-2]

        if (zeroOne and (not plusOne) and (not minusOne)):
            return False

        if (plusOne and minusOne):
            return False

        return True

    def scalarProduct(self, v1, v2):
        return v1[0] * v2[0] + v1[1] * v2[1]

    def correctNormal(self, normal, cutter, index):
        sideTop = cutter[index] 

        # Ищем подходящую вершину для проверки внутренней нормали
        for i in range(len(cutter)):
            top = cutter[(i + index + 2) % len(cutter)]
            v = [top[0] - sideTop[0], top[1] - sideTop[1]]  
            result = self.scalarProduct(v, normal) 

            if (result < 0):
                normal[0] = -normal[0]
                normal[1] = -normal[1] 
            elif (result > 0):
                break

    def getNormalVector(self, point1, point2):
        normal = []
        side = [point2[0] - point1[0], point2[1] - point1[1]]

        if side[0] == 0:
            normal = [1, 0]
        else:
            normal = [-side[1] / side[0], 1]

        return normal

    def cutLine(self, p1, p2, cutter):
        tDown = 0
        tUp = 1
        d = [p2[0] - p1[0], p2[1] - p1[1]]

        for i in range(len(cutter) - 1):
            w = [p1[0] - cutter[i][0], p1[1] - cutter[i][1]]
            normal = self.getNormalVector(cutter[i], cutter[i + 1])
            self.correctNormal(normal, self.cutter, i)
            wScalar = self.scalarProduct(normal, w)
            dScalar = self.scalarProduct(normal, d)

            if dScalar == 0:
                # Если точка невидима
                if wScalar < 0:
                    return
                else:
                    continue

            t = -wScalar / dScalar

            if dScalar > 0:
                if t > 1:
                    return
                else:
                    tDown = max(tDown, t)
            else:
                if t < 0:
                    return
                else:
                    tUp = min(tUp, t) 

        if (tDown < tUp):
            self.brezenhamInt(self.mainImage, self.convertValue(p1, p2, tDown),
                                              self.convertValue(p1, p2, tUp),
                                              self.cutLineColor)
        
            self.showClean(p1, p2, self.convertValue(p1, p2, tDown), self.convertValue(p1, p2, tUp), tDown, tUp)

    def convertValue(self, p1, p2, t):
        return [p1[0] + (p2[0] - p1[0]) * t, p1[1] + (p2[1] - p1[1]) * t]

    def cut(self):
        if (self.polygonIsConvex(self.cutter)):
            self.cutter.append(self.cutter[0])

            for i in range(len(self.lines) // 2):
                self.cutLine(self.lines[2 * i], self.lines[2 * i + 1], self.cutter)

            self.cutter = self.cutter[:-1]
        else:
            showwarning("Ошибка", "Многоугольник не является выпуклым!")

    def showClean(self, p1, p2, cutTopDown, cutTopUp, tDown, tUp):
        self.brezenhamInt(self.mainImage, p1, p2, self.backgroundColor)

        if (tDown != 0):
            self.brezenhamInt(self.mainImage, p1, cutTopDown, self.lineColor)

        if (tUp != 1):
            self.brezenhamInt(self.mainImage, p2, cutTopUp, self.lineColor)

        self.brezenhamInt(self.mainImage, cutTopDown, cutTopUp, self.cutLineColor)
        
        
        