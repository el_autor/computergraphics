from tkinter import*
from tkinter.messagebox import*
from tkinter.colorchooser import askcolor
import time

class Window:

    def __init__(self, canvasWnd):
        self.mainWindow = Tk()
        self.mainWindow.geometry('500x570+100+300')
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Лабораторная работа №8")

        self.initIndicator = IntVar(self.mainWindow)
        self.initIndicator.set(1)
        self.linesIndicator = Radiobutton(self.mainWindow, text = " - отрезки", variable = self.initIndicator, value = 0)
        self.cutterIndicator = Radiobutton(self.mainWindow, text = " - отсекатель", variable = self.initIndicator, value = 1)
        self.linesIndicator.place(x = 140, y = 120)
        self.cutterIndicator.place(x = 245, y = 120)

        self.initButton = Button(self.mainWindow, text = "Начать рисование", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.draw)
        self.initButton.place(x = 160, y = 150)   

        self.chooseColorButton = Button(self.mainWindow, text = "Выбрать цвет", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.setColor)
        self.chooseColorButton.place(x = 160, y = 230)

        self.cutButton = Button(self.mainWindow, text = "Отсечь", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.cut)
        self.cutButton.place(x = 160, y = 280)

        self.cleanButton = Button(self.mainWindow, text = "Очистка", width = 20, height = 2, bg = "white", bd = 1, relief = RIDGE, command = self.resetBackground)
        self.cleanButton.place(x = 160, y = 330)    

        self.canvasHandler = canvasWnd

        self.colorIndicator = IntVar(self.mainWindow)
        self.colorIndicator.set(0)
        self.colorIndicatorCutter = Radiobutton(self.mainWindow, text = " - отсекатель", variable = self.colorIndicator, value = 0)
        self.colorIndicatorLines = Radiobutton(self.mainWindow, text = " - отрезки", variable = self.colorIndicator, value = 1)
        self.colorIndicatorCutLines = Radiobutton(self.mainWindow, text = "- отсеченные отрезки", variable = self.colorIndicator, value = 2)
        self.colorIndicatorCutter.place(x = 70, y = 205)
        self.colorIndicatorLines.place(x = 195, y = 205)
        self.colorIndicatorCutLines.place(x = 300, y = 205)

    def draw(self):
        if (self.initIndicator.get()== 0):
            self.canvasHandler.canvas.bind("<Button 1>", self.canvasHandler.addLinePoint)
            self.canvasHandler.canvas.bind("<Shift-Button 1>", self.canvasHandler.addStraightLinePoint)
            self.canvasHandler.canvas.bind("<Shift-Button 3>", self.canvasHandler.addParallelLinePoint)
        else:
            self.canvasHandler.canvas.bind("<Button 1>", self.canvasHandler.addCutterSide)
            self.canvasHandler.canvas.bind("<Shift-Button 1>", self.canvasHandler.addStraightCutterSide)
            self.canvasHandler.canvas.bind("<Button 3>", self.canvasHandler.finishCutter)
   
    def setColor(self):
        color = askcolor(parent = self.mainWindow)

        if self.colorIndicator.get() == 0:
            self.canvasHandler.cutterColorRGB = (int(color[0][0]), int(color[0][1]),int(color[0][2]))
            self.canvasHandler.cutterColor = color[1]
        elif self.colorIndicator.get() == 1:
            self.canvasHandler.lineColorRGB = int(color[0][0]), int(color[0][1]),int(color[0][2])
            self.canvasHandler.lineColor = color[1]
        else:
            self.canvasHandler.cutLineColorRGB = int(color[0][0]), int(color[0][1]),int(color[0][2])
            self.canvasHandler.cutLineColor = color[1]

    def cut(self):
        self.canvasHandler.cut()

    def resetBackground(self):
        self.canvasHandler.fillBackground()
        self.canvasHandler.lines = []
        self.canvasHandler.cutter = []
        self.canvasHandler.lineInputFlag = False
        self.canvasHandler.cutterInputFlag = True
        self.canvasHandler.canvas.unbind("<Button 1>")
        self.canvasHandler.canvas.unbind("<Shift-Button 1>")
        self.canvasHandler.canvas.unbind("<Button 3>")
        self.canvasHandler.canvas.unbind("<Shift-Button 3>")

    def loop(self):
        self.mainWindow.mainloop()