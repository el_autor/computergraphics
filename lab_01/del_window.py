from tkinter import*
from tkinter.messagebox import*
import entry_checks

class DelWindow:

    def __init__(self, mainWindow):
        self.delWindow = Tk()
        self.delWindow.title("Удаление точек")
        self.delWindow.geometry("200x160")
        self.delWindow.resizable(False, False)

        self.delBut = Button(self.delWindow, text = "Удалить", width = 20, height = 1, bg = "white", bd = 3, command = self.delete_by_index)
        self.delBut.place(x = 15, y = 75)

        self.delAll = Button(self.delWindow, text = "Удалить все точки", width = 20, height = 1, bg = "white", bd = 3, command = self.delete_all)
        self.delAll.place(x = 15, y = 115)

        self.delIndicator = Label(self.delWindow, text = "Введите номер точки", width = 20, height = 1, bd = 3)
        self.delIndicator.place(x = 25, y = 10)

        self.entry = Entry(self.delWindow, width = 8, bd = 3, bg = "white")
        self.entry.place(x = 65, y = 40)

        self.mainWnd = mainWindow

    def delete_by_index(self):
        data = self.entry.get()

        if (data == ""):
            showwarning("Предупреждение", "Введена пустая строка!")
        elif (not (entry_checks.is_int(data))):
            showwarning("Предупреждение", "Введите целое число!")
        else:
            if (self.mainWnd.indicatorVar.get() == 0):
                if (entry_checks.is_proper_index(int(data), self.mainWnd.boxA)):
                    self.mainWnd.boxA.delete(int(data) - 1)
                    del self.mainWnd.setApoints[int(data) - 1]
                    entry_checks.offset(self.mainWnd.boxA, int(data) - 1)
                else:
                    showwarning("Предупреждение", "Введен ошибочный индекс!")
            else:
                if (entry_checks.is_proper_index(int(data), self.mainWnd.boxB)):
                    self.mainWnd.boxB.delete(int(data) - 1)
                    del self.mainWnd.setBpoints[int(data) - 1]
                    entry_checks.offset(self.mainWnd.boxB, int(data) -1)
                else:
                    showwarning("Предупреждение", "Введен ошибочный индекс!")

    def delete_all(self):
        if (self.mainWnd.indicatorVar.get() == 0):
            while (self.mainWnd.boxA.size() != 0):
                self.mainWnd.boxA.delete(0)

            self.mainWnd.setApoints = []
        else:
            while (self.mainWnd.boxB.size() != 0):
                self.mainWnd.boxB.delete(0)

            self.mainWnd.setBpoints = []