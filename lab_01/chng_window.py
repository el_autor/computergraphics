from tkinter import*
from tkinter.messagebox import*
import entry_checks

class ChangeWindow:

    def __init__(self, mainWindow):
        self.chngWindow = Tk()
        self.chngWindow.title("Изменение точки")
        self.chngWindow.geometry("200x160")
        self.chngWindow.resizable(False, False)

        self.indicator = Label(self.chngWindow, text = "Введите номер точки", width = 20, bd = 3)
        self.indicator.place(x = 27, y = 10)

        self.labelX = Label(self.chngWindow, text = "X", width = 10, height = 1)
        self.labelY = Label(self.chngWindow, text = "Y", width = 10, height = 1)
        self.labelX.place(x = 18, y = 65)
        self.labelY.place(x = 108, y = 65)

        self.inputNumber = Entry(self.chngWindow, width = 10, bd = 3)
        self.inputX = Entry(self.chngWindow, width = 10, bd = 3)
        self.inputY = Entry(self.chngWindow, width = 10, bd = 3)
        self.inputNumber.place(x = 57, y = 35)
        self.inputX.place(x = 15, y = 90)
        self.inputY.place(x = 105, y = 90)

        self.chngButton = Button(self.chngWindow, text = "Изменить", width = 20, height = 1, bg = "white", bd = 3, command = self.change)
        self.chngButton.place(x = 15, y = 120)

        self.mainWnd = mainWindow

    def replace(self, number, coordX, coordY, box):
        box.delete(int(number) - 1)
        box.insert(int(number) - 1, number + ") (" + coordX + ", " + coordY + ")")

    def change(self):
        number = self.inputNumber.get()
        coordX = self.inputX.get()
        coordY = self.inputY.get()

        if (not (entry_checks.is_int(number))):
            showwarning("Предупреждение", "Номер точки - целое число")
        elif (not (entry_checks.is_float(coordX)) or not (entry_checks.is_float(coordY))):
            showwarning("Предупреждение", "Координата точки - вещественное число")
        else:
            if (self.mainWnd.indicatorVar.get() == 0):
                if (entry_checks.is_proper_index(int(number), self.mainWnd.boxA)):
                    self.replace(number, coordX, coordY, self.mainWnd.boxA)
                    self.mainWnd.setApoints[int(number) - 1][0] = float(coordX)
                    self.mainWnd.setApoints[int(number) - 1][1] = float(coordY)
                else:
                    showwarning("Предупреждение", "Неверный номер точки")
            else:
                if (entry_checks.is_proper_index(int(number), self.mainWnd.boxB)):
                    self.replace(number, coordX, coordY, self.mainWnd.boxB)
                    self.mainWnd.setBpoints[int(number) - 1][0] = float(coordX)
                    self.mainWnd.setBpoints[int(number) - 1][1] = float(coordY)

                else:
                    showwarning("Предупреждение", "Неверный номер точки")