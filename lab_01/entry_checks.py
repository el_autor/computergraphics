def is_float(string):
    try:
        local = float(string)
    except ValueError:
        return False

    return True


def is_int(data):
    try:
        local = int(data)
    except ValueError:
        return False
    
    return True

def is_proper_index(index, box):
    if (index < 1):
        return False
    elif (index > box.size()):
        return False

    return True

def offset(box, deleted_index):
    for i in range(deleted_index, box.size()):
        local = box.get(i)
        box.delete(i)
        box.insert(i, str(i + 1) + ")" + local[2:])
