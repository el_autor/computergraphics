from tkinter import*
from tkinter.messagebox import*
import math

class MyCanvas:

    def __init__(self, mainWindow):
        self.canvasWindow = Tk()
        self.canvasWindow.title("Рисунок")
        self.canvasWindow.geometry("1200x1000")
        self.canvasWindow.resizable(False, False)

        self.width = 1198
        self.height = 998

        self.canvas = Canvas(self.canvasWindow, width = self.width, height = self.height, bg = "white")
        self.canvas.place(x = 0, y = 0)

        self.mainWnd = mainWindow
        self.eps = 0.000000001

        self.kX = min(self.get_kx(self.mainWnd.setApoints), self.get_kx(self.mainWnd.setBpoints))
        self.kY = min(self.get_ky(self.mainWnd.setApoints), self.get_ky(self.mainWnd.setBpoints))
        self.k = min(self.kX, self.kY)

        self.approximation = 1000000

    def draw_line(self, pointA, pointB, color):
        self.canvas.create_line(pointA[0] * self.k, self.height - pointA[1] * self.k, pointB[0] * self.k, self.height - pointB[1] * self.k, width = 2, fill = color)

    def draw_point(self, coords, color):
        self.canvas.create_oval(coords[0] * self.k - 3, self.height - coords[1] * self.k - 3, coords[0] * self.k + 3, self.height - coords[1] * self.k + 3, width = 2, outline = color, fill = color)

    def draw_points(self, points_data, color):
        for point in points_data:
            self.draw_point(point, color)

    def get_kx(self, points):
        max_x = points[0][0]

        for i in range(1, len(points)):
            if (points[i][0] > max_x):
                max_x = points[i][0]

        if (abs(max_x) <= self.eps):
            return 0
        else:
            return self.width / max_x

    def get_ky(self, points):
        max_y = points[0][1]

        for i in range(1, len(points)):
            if (points[i][1] > max_y):
                max_y = points[i][1]

        if (abs(max_y) <= self.eps):
            return 0
        else:
            return self.height / max_y

    def circle_available(self, pointA, pointB, pointC):
        sideA = self.get_line_width(pointA, pointB)
        sideB = self.get_line_width(pointB, pointC)
        sideC = self.get_line_width(pointC, pointA)

        if (abs(sideA) <= self.eps or abs(sideB) < self.eps or abs(sideC) < self.eps):
            return False

        if (abs(sideA + sideB - sideC) <= self.eps or abs(sideB + sideC - sideA) <= self.eps or abs(sideC + sideA - sideB) <= self.eps):
            return False

        return True

    def get_line_width(self, pointA, pointB):
        return (math.sqrt(((pointA[0] - pointB[0])**2) + (pointA[1] - pointB[1])**2))

    def get_line_koef(self, pointA, pointB):
        if (abs(pointB[0] - pointA[0]) <= self.eps):
            return self.approximation
        else:
            return (pointB[1] - pointA[1]) / (pointB[0] - pointA[0]) 

    def get_radius(self, basePoint, center):
        return self.get_line_width(basePoint, center)

    def get_circle_data(self, pointA, pointB, pointC):
        midSideA = [pointA[0] + (pointB[0] - pointA[0]) / 2, pointA[1] + (pointB[1] - pointA[1]) / 2]   
        midSideB = [pointB[0] + (pointC[0] - pointB[0]) / 2, pointB[1] + (pointC[1] - pointB[1]) / 2]   

        koefA = self.get_line_koef(pointA, pointB)
        koefB = self.get_line_koef(pointB, pointC)

        # Коэффициенты k: y = kx + b срединных перпендикуляров

        #print(pointA, pointB, pointC)
        #print(koefB)

        if (abs(koefA) <= self.eps):
            normalA = self.approximation
        else: 
            normalA = -1 / koefA

        if (abs(koefB) <= self.eps):
            normalB = self.approximation
        else:
            normalB = -1 / koefB

        # Коэффициенты в уравнении срединного перпенликуляра y = kx + b
        bA = midSideA[1] - normalA * midSideA[0]
        bB = midSideB[1] - normalB * midSideB[0]

        circleCenter_x = (bB - bA) / (normalA - normalB)
        circleCenter_y = normalA * circleCenter_x + bA

        rad = self.get_radius(pointA, [circleCenter_x, circleCenter_y])

        return [circleCenter_x, circleCenter_y], rad

    def draw_circle(self, centerPoint, rad, color):    
        self.canvas.create_oval((centerPoint[0] - rad) * self.k, self.height - centerPoint[1] * self.k - rad * self.k,
                                (centerPoint[0] + rad) * self.k, self.height - centerPoint[1] * self.k + rad * self.k, outline = color, width = 2)

    def get_tangent_points(self, centerA, radA, centerB, radB):
        centerConnect = self.get_line_width(centerA, centerB)
        proportion_k = radA / radB
        lengthPartA = (proportion_k * centerConnect) / (1 + proportion_k)
        lengthPartB = centerConnect - lengthPartA

        pointB = [centerA[0] + (centerB[0] - centerA[0]) * (lengthPartA / centerConnect), centerA[1] + (centerB[1] - centerA[1]) * (lengthPartA / centerConnect)]
        sideBC = math.sqrt(lengthPartA**2 - radA**2)
        D = 4 * (sideBC**2 * centerA[0] + radA**2 * pointB[0])**2 - 4 * (sideBC**2 + radA**2) * (centerA[0]**2 * sideBC**2 + pointB[0]**2 * radA**2 - radA**2 * sideBC**2)
        
        #print(D)
        if (D <= self.eps * self.eps):
            D = 0

        tangentPointx1 = (2 * (sideBC**2 * centerA[0] + radA**2 * pointB[0]) + math.sqrt(D)) / (2 * (sideBC**2 + radA**2))
        tangentPointx2 = (2 * (sideBC**2 * centerA[0] + radA**2 * pointB[0]) - math.sqrt(D)) / (2 * (sideBC**2 + radA**2)) 
        
        if (abs(pointB[1] - centerA[1]) <= self.eps):
            tangentPointy1 = centerA[1] + radA * (sideBC / lengthPartA) 
            tangentPointy2 = centerA[1] - radA * (sideBC / lengthPartA)
        else:
            tangentPointy1 = (radA**2 - sideBC**2 + 2 * tangentPointx1 * (centerA[0] - pointB[0]) - centerA[0]**2 + pointB[0]**2 - centerA[1]**2 + pointB[1]**2) / (2 * (pointB[1] - centerA[1]))
            tangentPointy2 = (radA**2 - sideBC**2 + 2 * tangentPointx2 * (centerA[0] - pointB[0]) - centerA[0]**2 + pointB[0]**2 - centerA[1]**2 + pointB[1]**2) / (2 * (pointB[1] - centerA[1]))

        sideBC = math.sqrt(lengthPartB**2 - radB**2)
        D = 4 * (sideBC**2 * centerB[0] + radB**2 * pointB[0])**2 - 4 * (sideBC**2 + radB**2) * (centerB[0]**2 * sideBC**2 + pointB[0]**2 * radB**2 - radB**2 * sideBC**2)

        #print(D)
        if (D <= self.eps * self.eps):
            D = 0

        tangentPointx3 = (2 * (sideBC**2 * centerB[0] + radB**2 * pointB[0]) + math.sqrt(D)) / (2 * (sideBC**2 + radB**2))
        tangentPointx4 = (2 * (sideBC**2 * centerB[0] + radB**2 * pointB[0]) - math.sqrt(D)) / (2 * (sideBC**2 + radB**2)) 

        if (abs(pointB[1] - centerB[1]) <= self.eps):
            tangentPointy3 = centerB[1] + radB * (sideBC / lengthPartB)
            tangentPointy4= centerB[1] - radB * (sideBC / lengthPartB)
        else:
            tangentPointy3 = (radB**2 - sideBC**2 + 2 * tangentPointx3 * (centerB[0] - pointB[0]) - centerB[0]**2 + pointB[0]**2 - centerB[1]**2 + pointB[1]**2) / (2 * (pointB[1] - centerB[1]))
            tangentPointy4 = (radB**2 - sideBC**2 + 2 * tangentPointx4 * (centerB[0] - pointB[0]) - centerB[0]**2 + pointB[0]**2 - centerB[1]**2 + pointB[1]**2) / (2 * (pointB[1] - centerB[1]))        

        return [[tangentPointx1, tangentPointy1], [tangentPointx2, tangentPointy2]], [[tangentPointx3, tangentPointy3], [tangentPointx4, tangentPointy4]], pointB

    def get_diff(self, centerA, centerB, separateConnectPoint, tangentPointsA, tangentPointsB):
        diagA1 = self.get_line_width(centerA, separateConnectPoint)
        diagA2 = self.get_line_width(tangentPointsA[0], tangentPointsA[1])

        areaA = 0.5 * diagA1 * diagA2

        diagB1 = self.get_line_width(centerB, separateConnectPoint)
        diagB2 = self.get_line_width(tangentPointsB[0], tangentPointsB[1])

        areaB = 0.5 * diagB1 * diagB2

        return abs(areaA - areaB)

    def place_label(self, label, point):
        up = down = left = right = False

        if (point[0] * self.k <= self.width - label.winfo_reqwidth() - 2):
            right = True
        else:
            left = True

        if (self.height - point[1] * self.k >= 30):
            up = True
        else:
            down = True

        if (right):
            labelX = point[0] * self.k + 5
        else:
            labelX = point[0] * self.k - label.winfo_reqwidth() - 3

        if (up):
            labelY = self.height - point[1] * self.k - 25 
        else:
            labelY = self.height - point[1] * self.k + 5 

        label.place(x = labelX, y = labelY)

    def draw_result(self, centerFirst, radFirst, centerSecond, radSecond, basePointsA, basePointsB, tangentPointsA, tangentPointsB):
        minX = min(centerFirst[0] - radFirst, centerSecond[0] - radSecond)
        maxX = max(centerFirst[0] + radFirst, centerSecond[0] + radSecond)

        minY = min(centerFirst[1] - radFirst, centerSecond[1] - radSecond)
        maxY = max(centerFirst[1] + radFirst, centerSecond[1] + radSecond)

        deltaX = 0 - minX
        deltaY = 0 - minY

        self.k = min(self.width / abs(minX - maxX), self.height / abs(minY - maxY))

        for i in range (len(basePointsA)):
            self.draw_point([basePointsA[i][0] + deltaX, basePointsA[i][1] + deltaY], "red") 

        for i in range (len(basePointsB)):
            self.draw_point([basePointsB[i][0] + deltaX, basePointsB[i][1] + deltaY], "green")

        self.draw_circle([centerFirst[0] + deltaX, centerFirst[1] + deltaY], radFirst, "red")
        self.draw_circle([centerSecond[0] + deltaX, centerSecond[1] + deltaY], radSecond, "green")

        self.draw_point([centerFirst[0] + deltaX, centerFirst[1] + deltaY], "red")
        self.draw_point([centerSecond[0] + deltaX, centerSecond[1] + deltaY], "green")

        self.draw_line([centerFirst[0] + deltaX, centerFirst[1] + deltaY], [centerSecond[0] + deltaX, centerSecond[1] + deltaY], "blue")

        for i in range(len(tangentPointsA)):
            self.draw_point([tangentPointsA[i][0] + deltaX, tangentPointsA[i][1] + deltaY] , "black")

        for i in range(len(tangentPointsB)):
            self.draw_point([tangentPointsB[i][0] + deltaX, tangentPointsB[i][1] + deltaY] , "black")

        # Рисуем внутренние касательные
        self.draw_line([tangentPointsA[0][0] + deltaX, tangentPointsA[0][1] + deltaY], [tangentPointsB[1][0] + deltaX, tangentPointsB[1][1] + deltaY], "yellow")
        self.draw_line([tangentPointsA[1][0] + deltaX, tangentPointsA[1][1] + deltaY], [tangentPointsB[0][0] + deltaX, tangentPointsB[0][1] + deltaY], "yellow")

        # Отрисовка прямых, соединяющих центры окружностей с точками касательных
        self.draw_line([tangentPointsA[0][0] + deltaX, tangentPointsA[0][1] + deltaY], [centerFirst[0] + deltaX, centerFirst[1] + deltaY], "yellow")
        self.draw_line([tangentPointsA[1][0] + deltaX, tangentPointsA[1][1] + deltaY], [centerFirst[0] + deltaX, centerFirst[1] + deltaY], "yellow")
        self.draw_line([tangentPointsB[0][0] + deltaX, tangentPointsB[0][1] + deltaY], [centerSecond[0] + deltaX, centerSecond[1] + deltaY], "yellow")
        self.draw_line([tangentPointsB[1][0] + deltaX, tangentPointsB[1][1] + deltaY], [centerSecond[0] + deltaX, centerSecond[1] + deltaY], "yellow")

        # Отмечаем точки с их координатами
        for i in range(len(basePointsA)):
            label = Label(self.canvas, text = "A" + str(i + 1) + " (" + str(basePointsA[i][0]) + ", " + str(basePointsA[i][1]) + ")" , bg = "white")
            self.place_label(label, [basePointsA[i][0] + deltaX, basePointsA[i][1] + deltaY])        

        for i in range(len(basePointsB)):
            label = Label(self.canvas, text = "B" + str(i + 1) + " (" + str(basePointsB[i][0]) + ", " + str(basePointsB[i][1]) + ")" , bg = "white")
            self.place_label(label, [basePointsB[i][0] + deltaX, basePointsB[i][1] + deltaY])

    def calc_result(self, setApoints, setBpoints):
        max_diff = -1

        if (len(setApoints) < 3 or len(setBpoints) < 3):
            showwarning("Предупреждение", "Решение не может быть получено!\
                         \nВ одном из множеств меньше 3-х точек!")
            return max_diff

        for i1 in range(len(setApoints)):
            for j1 in range(i1 + 1, len(setApoints)):
                for k1 in range(j1 + 1, len(setApoints)):
                    if (self.circle_available(setApoints[i1], setApoints[j1], setApoints[k1])):
                        centerA, radA = self.get_circle_data(setApoints[i1], setApoints[j1], setApoints[k1])

                        for i2 in range(len(setBpoints)):
                            for j2 in range(i2 + 1, len(setBpoints)):
                                for k2 in range(j2 + 1, len(setBpoints)):
                                    if (self.circle_available(setBpoints[i2], setBpoints[j2], setBpoints[k2])):
                                        centerB, radB = self.get_circle_data(setBpoints[i2], setBpoints[j2], setBpoints[k2])

                                        if (self.get_line_width(centerA, centerB) <= radA + radB):
                                            break
                                            
                                        tangentPointsA, tangentPointsB, separateConnectPoint = self.get_tangent_points(centerA, radA, centerB, radB)
                                        diff = self.get_diff(centerA, centerB, separateConnectPoint, tangentPointsB, tangentPointsB)

                                        if (diff > max_diff):
                                            max_diff = diff
                                            centerAmax, radAmax = centerA, radA
                                            centerBmax, radBmax = centerB, radB
                                            basePointsAmax = [setApoints[i1], setApoints[j1], setApoints[k1]]
                                            basePointsBmax = [setBpoints[i2], setBpoints[j2], setBpoints[k2]]
                                            tangentPointsAmax = tangentPointsA
                                            tangentPointsBmax = tangentPointsB

        if (max_diff > -1):
            self.draw_result(centerAmax, radAmax, centerBmax, radBmax, basePointsAmax, basePointsBmax, tangentPointsAmax, tangentPointsBmax)
        else:
            self.canvasWindow.destroy()
            showwarning("Предупреждение", "Решение не может быть получено!")

        return max_diff