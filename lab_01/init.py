from tkinter import*
from tkinter.messagebox import*
import add_window
import del_window
import chng_window
import canvas

class Window():

    def __init__(self):
        self.mainWindow = Tk()
        self.mainWindow.geometry('600x400')
        self.mainWindow.resizable(False, False)
        self.mainWindow.title("Лабораторная работа №1")

        self.addBut = Button(self.mainWindow, text = "Добавить", width = 20, height = 2, bg = "white", bd = 3, command = self.add_point)
        self.addBut.place(x = 20, y = 120)

        delBut = Button(self.mainWindow, text = "Удалить", width = 20, height = 2, bg = "white", bd = 3, command = self.del_point)
        delBut.place(x = 20, y = 180)

        chngBut = Button(self.mainWindow, text = "Изменить", width = 20, height = 2, bg = "white", bd = 3, command = self.change_point)
        chngBut.place(x = 20, y = 240)

        resultBut = Button(self.mainWindow, text = "Нарисовать", width = 20, height = 1, bg = "white", bd = 3, command = self.draw)
        resultBut.place(x = 215, y = 340)

        labelIndicator = Label(self.mainWindow, text = "Изменяемое множество", width = 25, height = 1)
        labelIndicator.place(x = 20, y = 15)

        labelA = Label(self.mainWindow, text = "A", width = 10, height = 1)
        labelA.place(x = 265, y = 15)

        labelB = Label(self.mainWindow, text = "B", width = 10, height = 1)
        labelB.place(x = 457, y = 15)

        self.boxA = Listbox(self.mainWindow, width = 20, height = 15, bd = 3)
        self.boxA.place(x = 230, y = 45)

        self.boxB = Listbox(self.mainWindow, width = 20, height = 15, bd = 3)
        self.boxB.place(x = 420, y = 45)
        self.boxB.size()

        self.indicatorVar = BooleanVar()
        self.indicatorVar.set(0)
        self.indicatorA = Radiobutton(self.mainWindow, text = "- A", variable = self.indicatorVar, value = 0)
        self.indicatorB = Radiobutton(self.mainWindow, text = "- B", variable = self.indicatorVar, value = 1)
        self.indicatorA.place(x = 20, y = 55)
        self.indicatorB.place(x = 20, y = 75)

        self.setApoints = []
        self.setBpoints = []

    def add_point(self):
        window = add_window.AddWindow(self)

    def del_point(self):
        window = del_window.DelWindow(self)

    def change_point(self):
        window = chng_window.ChangeWindow(self)

    def draw(self):
        myCanvas = canvas.MyCanvas(self)
        max_diff = myCanvas.calc_result(self.setApoints, self.setBpoints)

        if (abs(max_diff) <= myCanvas.eps):
            max_diff = 0

        if (max_diff != -1):
            showinfo("Результат", "Максимальная разность\n" + str(max_diff))

    def loop(self):
        self.mainWindow.mainloop()