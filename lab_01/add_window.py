from tkinter import*
from tkinter.messagebox import*
import entry_checks

class AddWindow:

    def __init__(self, mainWindow):
        self.addWindow = Tk()
        self.addWindow.title("Ввод новой точки")
        self.addWindow.geometry("200x130")
        self.addWindow.resizable(False, False)

        self.inputBut = Button(self.addWindow, text = "Добавить точку", width = 12, height = 1, bd = 3, bg = "white", command = self.add)
        self.inputBut.place(x = 45, y = 75)

        self.labelX = Label(self.addWindow, text = "X", width = 10, height = 1)
        self.labelY = Label(self.addWindow, text = "Y", width = 10, height = 1)
        self.labelX.place(x = 15, y = 15)
        self.labelY.place(x = 105, y = 15)

        self.entryX = Entry(self.addWindow, width = 10, bg = "white", bd = 3)
        self.entryY = Entry(self.addWindow, width = 10, bg = "white", bd = 3)
        self.entryX.place(x = 10, y = 40)
        self.entryY.place(x = 105, y = 40)

        self.mainWnd = mainWindow

    def apply_data(self, x , y, data, box):
        box.insert(END, str(box.size() + 1) + ") (" + str(x) + ", " + str(y) + ")")
        data.append([x, y])

    def add(self):
        x = self.entryX.get()
        y = self.entryY.get()

        #print(self.mainWnd.setApoints)
        #print(self.mainWnd.setBpoints)

        if (x == "" or y == ""):
            showwarning("Предупреждение", "Координаты не введены!")
        elif (not (entry_checks.is_float(x)) or not entry_checks.is_float(y)):
            showwarning("Предупреждение", "Координаты введены неверно!")
        else:
            if (self.mainWnd.indicatorVar.get() == 0):
                self.apply_data(float(x), float(y), self.mainWnd.setApoints, self.mainWnd.boxA)
            else:
                self.apply_data(float(x), float(y), self.mainWnd.setBpoints, self.mainWnd.boxB)